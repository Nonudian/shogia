# ShogIA

***fr:*** Notre projet a pour objectif la mise en oeuvre d'une modélisation distribuée, à travers le jeu de Shogi.  
***en:*** Our project was designed to simulate a multi-agent system using a distributed modelling, through a Shogi game.

Demo : https://www.youtube.com/watch?v=QOfznwUUy8Q