package control;

import static javafx.animation.Animation.Status.RUNNING;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.LinkedBlockingQueue;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.geometry.Point2D;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import javafx.util.Duration;
import logic.message.ContentMessage;
import logic.move.Move;
import logic.piece.King;
import logic.piece.Piece;
import logic.piece.Promotable;
import logic.util.ActionLogger;
import logic.util.Team;
import renderer.MainRenderer;

public class ShogIAApp extends Application {

    public LinkedBlockingQueue<ContentMessage<Move>> messages = new LinkedBlockingQueue<>();

    public static Team currentTeam;
    private Team defaultTeam;
    private final Team[] teams = Team.values();
    private final MainRenderer renderer = new MainRenderer(teams);
    public static int turn = 1;
    private boolean endGame = false;

    private static final int TIMELINE_DURATION = 1;
    private Timeline timeline;

    public static void main(String[] args) {
        launch(args);
    }

    /**
     * Starts and initializes the stage, i.e. teams, messenger manager, timeline, and the scene.
     * @param stage the stage to initialize
     */
    @Override
    public void start(Stage stage) {
        initTeamsConfig();
        initTimeline();
        initStage(stage);
        initLogger();
    }

    /**
     * Initializes the top and bottom teams.
     */
    private void initTeamsConfig() {
        int teamIndex = new Random().nextInt(teams.length);
        currentTeam = defaultTeam = teams[teamIndex];
        currentTeam.getKing().init(this);
        currentTeam.getOpposite().getKing().init(this);
    }

    /**
     * Initializes the graphical timeline.
     */
    private void initTimeline() {
        timeline = new Timeline(new KeyFrame(Duration.millis(TIMELINE_DURATION), event -> refreshApp()));
        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.play();
    }

    /**
     * Initializes the scene of this application stage, and some stage properties.
     * @param stage the stage to initialize
     */
    private void initStage(Stage stage) {
        Scene scene = new Scene(renderer);
        scene.addEventFilter(KeyEvent.KEY_RELEASED, event -> {
            if (event.getCode() == KeyCode.SPACE) {
                if (isRunning()) {
                    timeline.pause();
                    stage.setTitle("ShogIA Project (PAUSED) | Press SPACE to UNPAUSE");
                } else if (!endGame) {
                    timeline.play();
                    stage.setTitle("ShogIA Project (RUNNING) | Press SPACE to PAUSE");
                }
            }
        });

        stage.setScene(scene);
        stage.setTitle("ShogIA Project (RUNNING) | Press SPACE to PAUSE");
        stage.setResizable(false);
        stage.setOnCloseRequest(event -> {
            currentTeam.getKing().shutdown();
            currentTeam.getOpposite().getKing().shutdown();
        });
        stage.show();
    }

    private void initLogger() {
        ActionLogger.setupLogger();
    }

    /**
     * Tests if this application is currently running.
     * @return a boolean that check the condition
     */
    private boolean isRunning() {
        return timeline.getStatus() == RUNNING;
    }

    /**
     * Swaps to the next team at the end of each turn.
     */
    private void switchNextTeam() {
        currentTeam = currentTeam.getOpposite();
        if (currentTeam.equals(defaultTeam)) turn++;
    }

    /**
     * Sending function of this application. Used to update graphical interface and chat with team leader.
     * Refresh the state of game, currently each second (precised with TIMELINE_DURATION).
     */
    private void refreshApp() {
        if (isRunning()) {
            // update current team information
            currentTeam.update();

            // launch team decision
            currentTeam.getKing().buildDecision();

            // waiting for the decision
            try {
                applyFinalDecision(messages.take().getContent());
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                // switch to next team
                switchNextTeam();
            }
        }
    }

    /**
     * Apply the final decision, for this turn, sent by the currentTeam's king.
     * Update the logic and the graphics.
     */
    private void applyFinalDecision(Move move) {
        Piece actor = move.getActor();
        Point2D finalPosition = move.getFinalPosition();
        Piece target = move.getTarget();

        if (move.hasTarget() && target instanceof King) {
            endGame = true;
            timeline.stop();
            ActionLogger.end(target, actor, actor.getPosition());

            printStatisticsOf(currentTeam);
            printStatisticsOf(currentTeam.getOpposite());
            return;
        }

        switch (move.getState()) {
            case FORWARD:
                this.applyForward(actor, finalPosition, false);
                break;
            case PROMOTING_MOVE:
                this.applyForward(actor, finalPosition, true);
                break;
            case CAPTURING_MOVE:
                this.applyCapture(actor, finalPosition, target, false);
                break;
            case CAPTURING_AND_PROMOTING_MOVE:
                this.applyCapture(actor, finalPosition, target, true);
                break;
            case DROP:
                this.applyDrop(actor, finalPosition);
                break;
        }
    }

    private void applyForward(Piece actor, Point2D finalPosition, boolean promotion) {
        ActionLogger.move(actor, actor.getPosition(), finalPosition);

        actor.setPosition(finalPosition);
        if(promotion) ((Promotable) actor).bePromoted();

        renderer.moveOnly(actor.getId(), promotion);
    }

    private void applyCapture(Piece actor, Point2D finalPosition, Piece target, boolean promotion) {
        ActionLogger.capture(target, actor, actor.getPosition());

        currentTeam.capture(target);
        target.beCaptured();
        actor.setPosition(finalPosition);
        if(promotion) ((Promotable) actor).bePromoted();

        renderer.moveAndCapture(actor.getId(), promotion, target.getId(), target.getLetter());
    }

    private void applyDrop(Piece actor, Point2D finalPosition) {
        ActionLogger.drop(actor, finalPosition);

        actor.setPosition(finalPosition);
        currentTeam.drop(actor);
        actor.beDropped();

        renderer.dropOnly(actor.getId(), actor.getLetter());
    }

    private void printStatisticsOf(Team team) {
        System.out.println("\nStatistics for " + team.name() + " team: ");
        team.statistics.entrySet().stream()
                .sorted(Map.Entry.comparingByKey())
                .forEach(entry -> System.out.println("Strategy name: " + entry.getKey() + " | x" + entry.getValue()));
    }
}
