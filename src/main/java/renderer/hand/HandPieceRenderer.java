package renderer.hand;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import renderer.piece.PieceShapeRenderer;

public class HandPieceRenderer extends HBox {

    private static final String TIMES_SYMBOL = "x";

    private int count = 1;

    public HandPieceRenderer(String letter, int orientationFactor) {
        setAlignment(Pos.CENTER_LEFT);
        setSpacing(18);

        PieceShapeRenderer pieceShapeRenderer = new PieceShapeRenderer(letter, orientationFactor);
        Label countText = new Label(TIMES_SYMBOL + count);
        countText.setFont(Font.font("trebuchet ms", FontWeight.BOLD, 20));
        countText.setPadding(new Insets(25, 0, 0, 0));

        getChildren().addAll(pieceShapeRenderer, countText);
    }

    public int getCount() {
        return count;
    }

    public void increaseCount() {
        updateCount(++count);
    }

    public void decreaseCount() {
        updateCount(--count);
    }

    private void updateCount(int count) {
        ((Label) getChildren().get(1)).setText(TIMES_SYMBOL + count);
    }
}
