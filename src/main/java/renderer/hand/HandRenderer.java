package renderer.hand;

import java.util.HashMap;
import java.util.Map;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import logic.util.Team;

public class HandRenderer extends StackPane {

    private static final String HAND_COLOR = "#F2C966";
    private static final int HAND_WIDTH = 120;
    private static final int HAND_HEIGHT = 450;

    private VBox pieceRenderers = new VBox();

    private Team team;
    private Map<String, HandPieceRenderer> counts = new HashMap<>();

    public HandRenderer(Pos alignment, Team team) {
        this.team = team;

        setPrefSize(HAND_WIDTH, HAND_HEIGHT);
        setAlignment(alignment);

        initPieceRenderers(alignment);

        getChildren().addAll(createHandShape(), pieceRenderers);
    }

    private void initPieceRenderers(Pos alignment) {
        pieceRenderers.setMaxSize(HAND_WIDTH - (HAND_WIDTH * 0.2), HAND_HEIGHT);
        pieceRenderers.setAlignment(alignment);
        pieceRenderers.setPadding(new Insets(0, 15, 25, 0));
        pieceRenderers.setSpacing(10);
    }

    private Rectangle createHandShape() {
        Rectangle handShape = new Rectangle(HAND_WIDTH - (HAND_WIDTH * 0.2), HAND_HEIGHT);
        handShape.setFill(Color.valueOf(HAND_COLOR));
        handShape.setStroke(Color.BLACK);
        return handShape;
    }

    public Team getTeam() {
        return team;
    }

    public void increaseCountOf(String letter) {
        if (counts.containsKey(letter)) {
            counts.get(letter).increaseCount();
            return;
        }

        HandPieceRenderer pieceRenderer = new HandPieceRenderer(letter, team.orientationFactor);
        counts.put(letter, pieceRenderer);
        pieceRenderers.getChildren().add(pieceRenderer);
    }

    public void decreaseCountOf(String letter) {
        HandPieceRenderer pieceRenderer = counts.get(letter);
        if (pieceRenderer.getCount() - 1 == 0) {
            counts.remove(letter);
            pieceRenderers.getChildren().remove(pieceRenderer);
            return;
        }

        pieceRenderer.decreaseCount();
    }
}
