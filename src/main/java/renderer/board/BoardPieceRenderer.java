package renderer.board;

import static renderer.board.BoardRenderer.TILE_SIZE;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;
import javafx.scene.text.Text;
import logic.piece.Piece;
import renderer.piece.PieceShapeRenderer;

public class BoardPieceRenderer extends PieceShapeRenderer {

    private Piece model;

    public BoardPieceRenderer(Piece model) {
        super(model.getLetter(), model.getTeam().orientationFactor);
        this.model = model;

        relocate();
    }

    public void relocate() {
        relocate(model.getPosition().getX() * TILE_SIZE, model.getPosition().getY() * TILE_SIZE);
    }

    public void reloadShape() {
        getChildren().clear();
        initOrientedCoordinates(model.getTeam().orientationFactor);
        initShapeRenderer(model.getLetter());
    }

    private void colorPiece(String color) {
        Polygon pieceShape = ((Polygon) getChildren().get(1));
        pieceShape.setStroke(Color.valueOf(color));
        Text letter = ((Text) getChildren().get(2));
        letter.setFill(Color.valueOf(color));
    }

    public void applyPromotionColor() {
        colorPiece("D60100");
    }

    public void applyDropColor() {
        colorPiece("4C4CFF");
    }
}
