package renderer.board;

import static logic.constant.Consts.HEIGHT;
import static logic.constant.Consts.WIDTH;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import javafx.scene.Group;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import logic.util.Team;

public class BoardRenderer extends Pane {

    public static final int TILE_SIZE = 75;
    private static final String TILE_COLOR = "#F4CE72";

    private Group tileGroup = new Group();
    private Group pieceGroup = new Group();

    private Map<Integer, BoardPieceRenderer> identifiers = new HashMap<>();

    public BoardRenderer(Team[] teams) {
        setPrefSize(WIDTH * TILE_SIZE, HEIGHT * TILE_SIZE);

        initTileRendering();
        initPieceRenderers(teams);

        getChildren().addAll(tileGroup, pieceGroup);
    }

    private void initTileRendering() {
        for (int y = 0; y < HEIGHT; y++) {
            for (int x = 0; x < WIDTH; x++) {
                Rectangle tile = new Rectangle(TILE_SIZE, TILE_SIZE, Color.valueOf(TILE_COLOR));
                tile.setStroke(Color.BLACK);
                tile.relocate(x * TILE_SIZE, y * TILE_SIZE);
                tileGroup.getChildren().add(tile);
            }
        }
    }

    private void initPieceRenderers(Team[] teams) {
        Arrays.stream(teams).forEach(team -> team.actives.forEach(piece -> {
            BoardPieceRenderer pieceRenderer = new BoardPieceRenderer(piece);
            identifiers.put(piece.getId(), pieceRenderer);
            pieceGroup.getChildren().add(pieceRenderer);
        }));
    }

    public void movePiece(int pieceId, boolean isPromoted) {
        BoardPieceRenderer pieceRenderer = identifiers.get(pieceId);
        pieceRenderer.relocate();
        if (isPromoted) pieceRenderer.applyPromotionColor();
    }

    public void capturePiece(int pieceId) {
        BoardPieceRenderer pieceRenderer = identifiers.get(pieceId);
        pieceRenderer.setVisible(false);
        pieceRenderer.managedProperty().bind(pieceRenderer.visibleProperty());
    }

    public void dropPiece(int pieceId) {
        BoardPieceRenderer pieceRenderer = identifiers.get(pieceId);
        pieceRenderer.setVisible(true);
        pieceRenderer.managedProperty().bind(pieceRenderer.visibleProperty());

        pieceRenderer.reloadShape();
        pieceRenderer.relocate();
        pieceRenderer.applyDropColor();
    }
}
