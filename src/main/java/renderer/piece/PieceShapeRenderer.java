package renderer.piece;

import static renderer.board.BoardRenderer.TILE_SIZE;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

public class PieceShapeRenderer extends StackPane {

    private static final double MAX_X = 45.0;
    private static final double MAX_Y = 50.0;
    private static final Double[] POLYGON_COORDS
            = { 0.0, 0.0, 7.0, MAX_X, 22.5, MAX_Y, 38.0, MAX_X, MAX_X, 0.0 };
    private static final String PIECE_COLOR = "#F9DFA4";
    private static final String LETTER_FONT = "georgia";

    private List<Double> orientedCoordinates;

    public PieceShapeRenderer(String letter, int orientationFactor) {
        initOrientedCoordinates(orientationFactor);
        initShapeRenderer(letter);
    }

    protected void initOrientedCoordinates(int orientationFactor) {
        orientedCoordinates = Arrays.stream(POLYGON_COORDS)
                .map(pos -> pos * orientationFactor)
                .collect(Collectors.toList());
    }

    protected void initShapeRenderer(String letter) {
        Polygon pieceShape = setPolygon(Color.valueOf(PIECE_COLOR), 0.5);
        Polygon pieceShadow = setPolygon(Color.BLACK, 0.52);
        Text pieceLetter = setPieceLetter(letter);

        getChildren().addAll(pieceShadow, pieceShape, pieceLetter);
    }

    private Polygon setPolygon(Color fillingColor, double translationFactor) {
        Polygon polygon = new Polygon();
        polygon.getPoints().addAll(orientedCoordinates);

        polygon.setFill(fillingColor);
        polygon.setStroke(Color.BLACK);
        polygon.setStrokeWidth(TILE_SIZE * 0.01);

        polygon.setTranslateX(TILE_SIZE * translationFactor - MAX_X * 0.5);
        polygon.setTranslateY(TILE_SIZE * translationFactor - MAX_Y * 0.5);

        return polygon;
    }

    private Text setPieceLetter(String pieceLetter) {
        Text letter = new Text(pieceLetter);

        letter.setFont(Font.font(LETTER_FONT, FontWeight.BOLD, 30));

        letter.setTranslateX(TILE_SIZE * 0.5 - MAX_X * 0.5);
        letter.setTranslateY(TILE_SIZE * 0.5 - MAX_Y * 0.5);

        return letter;
    }
}
