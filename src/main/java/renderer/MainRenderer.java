package renderer;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import logic.util.Team;
import renderer.board.BoardRenderer;
import renderer.hand.HandRenderer;
import static control.ShogIAApp.currentTeam;

public class MainRenderer extends HBox {

    private static final String BACKGROUND_COLOR = "#C3C3C3";

    private BoardRenderer boardRenderer;
    private HandRenderer topHandRenderer;
    private HandRenderer bottomHandRenderer;

    public MainRenderer(Team[] teams) {
        boardRenderer = new BoardRenderer(teams);
        topHandRenderer = new HandRenderer(Pos.TOP_CENTER, teams[0]);
        bottomHandRenderer = new HandRenderer(Pos.BOTTOM_CENTER, teams[1]);
        initMainRendering();
    }

    private void initMainRendering() {
        setBackground(
                new Background(
                        new BackgroundFill(
                                Color.valueOf(BACKGROUND_COLOR),
                                CornerRadii.EMPTY,
                                Insets.EMPTY)));
        setPrefSize(
                topHandRenderer.getPrefWidth()
                        + boardRenderer.getPrefWidth()
                        + bottomHandRenderer.getPrefWidth(),
                boardRenderer.getPrefHeight());

        getChildren().addAll(topHandRenderer, boardRenderer, bottomHandRenderer);
    }

    public void moveOnly(int pieceId, boolean isPromoted) {
        boardRenderer.movePiece(pieceId, isPromoted);
    }
    public void moveAndCapture(int pieceId, boolean isPromoted, int targetId, String letter) {
        boardRenderer.capturePiece(targetId);
        boardRenderer.movePiece(pieceId, isPromoted);

        HandRenderer handRenderer = currentTeam.equals(topHandRenderer.getTeam()) ? topHandRenderer : bottomHandRenderer;
        handRenderer.increaseCountOf(letter);
    }
    public void dropOnly(int pieceId, String letter) {
        boardRenderer.dropPiece(pieceId);

        HandRenderer handRenderer = currentTeam.equals(topHandRenderer.getTeam()) ? topHandRenderer : bottomHandRenderer;
        handRenderer.decreaseCountOf(letter);
    }
}
