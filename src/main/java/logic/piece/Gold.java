package logic.piece;

import java.util.List;

import javafx.geometry.Point2D;
import logic.move.DropUtilities;
import logic.move.ForwardUtilities;
import logic.move.Motion;
import logic.util.Team;

public final class Gold extends Piece {

    public Gold(String letter, Point2D position, Team team) {
        super(letter, position, team, 6.90, 7.80);
    }

    @Override
    public List<Motion> getForwardPositions(Point2D position, Point2D... toExclude) {
        return ForwardUtilities.goldPositions(position, team, toExclude);
    }

    @Override
    protected List<Motion> getDropPositions() {
        return DropUtilities.defaultPositions(team);
    }
}
