package logic.piece;

import java.util.List;

import javafx.geometry.Point2D;
import logic.move.DropUtilities;
import logic.move.ForwardUtilities;
import logic.move.Motion;
import logic.util.Team;

public final class Rook extends Promotable {

    public Rook(String letter, Point2D position, Team team) {
        super(letter, position, team, 10.40, 12.70, 13.00);
    }

    @Override
    public List<Motion> getForwardPositions(Point2D position, Point2D... toExclude) {
        return (isPromoted)
                ? ForwardUtilities.promotedRookPositions(position, team, toExclude)
                : ForwardUtilities.rookPositions(position, team, toExclude);
    }

    @Override
    protected List<Motion> getDropPositions() {
        return DropUtilities.defaultPositions(team);
    }

    @Override
    protected boolean shouldPromote(Point2D movePosition) {
        return false;
    }
}
