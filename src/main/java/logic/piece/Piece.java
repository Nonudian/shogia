package logic.piece;

import static control.ShogIAApp.turn;
import static java.util.function.Predicate.isEqual;
import static java.util.function.Predicate.not;
import static logic.message.MessageType.ACK;
import static logic.message.MessageType.COLLECTIVE_CONCERNED;
import static logic.message.MessageType.COLLECTIVE_UNCONCERNED;
import static logic.message.MessageType.OPENING_CONCERNED;
import static logic.message.MessageType.SEND_NOTHING;
import static logic.message.MessageType.SEND_PAIR;
import static logic.message.MessageType.STRATEGY_CONCERNED;
import static logic.strategy.StrategyType.proactives;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.stream.Collectors;
import javafx.geometry.Point2D;
import javafx.util.Pair;
import logic.message.ContentMessage;
import logic.message.Message;
import logic.move.Motion;
import logic.move.Move;
import logic.strategy.Collective;
import logic.strategy.Strategy;
import logic.strategy.StrategyPair;
import logic.strategy.StrategyType;
import logic.util.Team;

public abstract class Piece {

    public LinkedBlockingQueue<Message> messages = new LinkedBlockingQueue<>();

    private static int idCounter = 0;
    protected int id = idCounter++;
    protected String letter;
    protected Point2D position;
    protected Team team;
    protected final double basicValuation;
    protected final double inHandValuation;
    protected double valuation;
    protected List<Move> moves = new ArrayList<>();
    protected Map<Point2D, List<Piece>> threats = new HashMap<>();

    protected List<Strategy> ownedStrategies = new ArrayList<>();
    protected List<Strategy> pendingStrategies = new ArrayList<>();
    protected List<StrategyPair> newPairs = new ArrayList<>();
    private Map<Integer, Pair<Point2D, Point2D>> alreadyPlayed = new HashMap<>();

    public Piece(String letter, Point2D position, Team team, double basicValuation, double inHandValuation) {
        this.letter = letter;
        this.position = position;
        this.team = team;
        this.basicValuation = basicValuation;
        this.inHandValuation = inHandValuation;
        this.valuation = basicValuation;
    }

    public int getId() {
        return id;
    }
    public String getLetter() {
        return letter;
    }
    public Point2D getPosition() {
        return position;
    }
    public Team getTeam() {
        return team;
    }
    public double getValuation() {
        return valuation;
    }
    public double getInHandValuation() {
        return inHandValuation;
    }

    public void beCaptured() {
        ownedStrategies.clear();
        pendingStrategies.clear();
        swapTeam();
        resetPosition();
        setInHandValuation();
        if (this instanceof Promotable) ((Promotable) this).beDemoted();
    }
    public void beDropped() {
        resetValuation();
    }

    private void swapTeam() {
        this.team.lastFallen = this;
        this.team = this.team.getOpposite();
    }
    private void resetPosition() {
        setPosition(null);
    }
    public void setPosition(Point2D position) {
        alreadyPlayed.put(turn + 1, new Pair<>(this.position, position));
        this.position = position;
    }
    private void setInHandValuation() {
        valuation = inHandValuation;
    }
    private void resetValuation() {
        valuation = basicValuation;
    }
    public boolean inHand() {
        return position == null;
    }
    public List<Move> getMoves() {
        return moves;
    }
    public boolean isThreatened() {
        return getThreats().isPresent();
    }
    public Optional<List<Piece>> getThreats() {
        return getThreatsFrom(position);
    }
    public Optional<List<Piece>> getThreatsFrom(Point2D position) {
        return Optional.ofNullable(threats.get(position));
    }

    /**
     * @return forward positions computed from piece-related formula and rules
     */
    public abstract List<Motion> getForwardPositions(Point2D position, Point2D... toExclude);

    /**
     * @return drop positions computed from piece-related formula and rules
     */
    protected abstract List<Motion> getDropPositions();

    /**
     * @return drop or forward positions, according to piece status (on board or in hand), indicated by position
     */
    public List<Motion> getCurrentMotions() {
        return inHand() ? getDropPositions() : getForwardPositions(position);
    }

    private List<Strategy> getOwnedPendingCollectives() {
        return pendingStrategies.stream()
                .filter(ownedStrategies::contains)
                .filter(Collective.class::isInstance)
                .collect(Collectors.toList());
    }

    private List<Strategy> getNotOwnedPendingCollectives() {
        return pendingStrategies.stream()
                .filter(not(ownedStrategies::contains))
                .collect(Collectors.toList());
    }

    private List<Collective> getOwnedNewCollectives() {
        return newPairs.stream()
                .map(StrategyPair::first)
                .filter(ownedStrategies::contains)
                .filter(Collective.class::isInstance)
                .map(Collective.class::cast)
                .collect(Collectors.toList());
    }

    private List<Strategy> getOwnedTerminatedPendingStrategies() {
        return pendingStrategies.stream()
                .filter(ownedStrategies::contains)
                .filter(Strategy::isTerminated)
                .collect(Collectors.toList());
    }

    private List<Strategy> getOwnedContinuedPendingStrategies() {
        return pendingStrategies.stream()
                .filter(ownedStrategies::contains)
                .filter(not(Strategy::isTerminated))
                .collect(Collectors.toList());
    }


    /**************************************
     *          CONTENT OF TASKS          *
     **************************************/

    protected Callable<Void> removingFallenPhase() {
        return () -> {
            removingFallen();

            writeACK();
            return null;
        };
    }

    protected void removingFallen() {
        pendingStrategies.removeAll(
                pendingStrategies.stream()
                        .filter(strategy -> strategy.getOwner().equals(team.lastFallen))
                        .collect(Collectors.toList()));
        getOwnedPendingCollectives().stream().map(Collective.class::cast)
                .forEach(strategy -> strategy.removeFallen(team.lastFallen));
    }

    @SuppressWarnings("unchecked")
    protected Callable<Void> openingPreparationPhase() {
        return () -> {
            Message message = messages.take();
            if (message.getType() == OPENING_CONCERNED)
                pendingStrategies.addAll(((ContentMessage<List<Strategy>>) message).getContent());

            writeACK();
            return null;
        };
    }

    protected Callable<Void> moveComputationPhase() {
        return () -> {
            threatComputation();
            moveComputation();

            writeACK();
            return null;
        };
    }

    protected void threatComputation() {
        threats.clear();
        team.getActiveEnemies().forEach(enemy -> {
            List<Point2D> toIgnore = enemy.getTeam().getAllExcepted(enemy)
                    .stream()
                    .filter(piece -> piece.getThreats().isPresent())
                    .filter(piece -> piece.getThreats().get().contains(this))
                    .map(Piece::getPosition)
                    .collect(Collectors.toList());
            toIgnore.add(position);

            enemy.getForwardPositions(enemy.getPosition(), toIgnore.toArray(Point2D[]::new)).forEach(motion -> {
                Point2D position = motion.getFinalPosition();

                if (threats.containsKey(position)) {
                    threats.get(position).add(enemy);
                } else {
                    List<Piece> initialList = new ArrayList<>();
                    initialList.add(enemy);
                    threats.put(position, initialList);
                }
            });
        });
    }

    /**
     * Updates moves. This method is called in thread start function, each game turn.
     */
    protected void moveComputation() {
        moves.clear();
        getCurrentMotions().forEach(motion -> {
            if (!alreadyPlayed.containsValue(new Pair<>(position, motion.getFinalPosition()))) {
                Move move = new Move(this, motion, inHand());

                team.getEnemyFrom(motion.getFinalPosition()).ifPresent(move::withCapture);
                getThreatsFrom(motion.getFinalPosition()).ifPresent(move::withRisk);

                moves.add(move);
            }
        });

        alreadyPlayed.keySet().removeIf(isEqual(turn));
    }

    protected Callable<Void> strategyMatchingPhase() {
        return () -> {
            strategyMatching();

            writeACK();
            return null;
        };
    }

    protected void strategyMatching() {
        ownedStrategies.stream()
                .filter(Collective.class::isInstance)
                .map(Collective.class::cast)
                .forEach(Collective::setAllies);
        ownedStrategies.forEach(Strategy::checkTermination);

        getOwnedContinuedPendingStrategies().forEach(strategy -> moves.forEach(move -> {
            if (strategy.isEligible(move)) newPairs.add(new StrategyPair(strategy, move));
        }));

        List<StrategyType> usedTypes = getOwnedContinuedPendingStrategies().stream().map(Strategy::getType).collect(Collectors.toList());
        proactives().stream()
                .filter(not(usedTypes::contains))
                .forEach(type -> moves.forEach(move -> {
                    Strategy strategy = Strategy.instantiate(type, this);
                    if (strategy.isEligible(move)) newPairs.add(new StrategyPair(strategy, move));
                }));
    }

    protected Callable<Void> newCollectiveSendingPhase() {
        return () -> {
            collectiveSending();

            writeACK();
            return null;
        };
    }

    protected void collectiveSending() {
        team.getAllExcepted(this).forEach(piece -> {
            List<Strategy> addingStrategies = new ArrayList<>();
            List<Strategy> withdrawingStrategies = new ArrayList<>();

            getOwnedNewCollectives().forEach(strategy -> {
                if (strategy.getAllies().contains(piece))
                    addingStrategies.add((Strategy) strategy);
            });

            getOwnedPendingCollectives().forEach(strategy -> {
                List<Piece> newParticipants = ((Collective) strategy).getAllies();
                List<Piece> oldParticipants = ((Collective) strategy).getOldAllies();

                if ((strategy.isTerminated() && oldParticipants.contains(piece)) ||
                        (oldParticipants.contains(piece) && !newParticipants.contains(piece)))
                    withdrawingStrategies.add(strategy);
                else if (!oldParticipants.contains(piece) && newParticipants.contains(piece))
                    addingStrategies.add(strategy);
            });

            if (addingStrategies.isEmpty() && withdrawingStrategies.isEmpty())
                write(piece, new Message(this, COLLECTIVE_UNCONCERNED));
            else {
                List<List<Strategy>> collectives = Arrays.asList(addingStrategies, withdrawingStrategies);
                write(piece, new ContentMessage<>(this, COLLECTIVE_CONCERNED, collectives));
            }
        });

        ownedStrategies.removeIf(Strategy::isTerminated);
        pendingStrategies.removeAll(getOwnedTerminatedPendingStrategies());
    }

    protected Callable<Void> newCollectiveReceivePhase() {
        return () -> {
            receiveCollective();

            writeACK();
            return null;
        };
    }

    @SuppressWarnings("unchecked")
    protected Callable<Void> moveSendingBestStrategyPair() {
        return () -> {
            if (newPairs.isEmpty()) {
                if (moves.isEmpty()) {
                    write(team.getKing(), new Message(this, SEND_NOTHING));
                } else {
                    Collections.sort(moves);
                    write(team.getKing(), new ContentMessage<>(this, SEND_PAIR, new StrategyPair(null, moves.get(0))));
                }
            } else {
                Collections.sort(newPairs);
                write(team.getKing(), new ContentMessage<>(this, SEND_PAIR, newPairs.get(0)));
            }

            Message message = messages.take();
            if (message.getType() == STRATEGY_CONCERNED) {
                Strategy strategy = ((ContentMessage<Strategy>) message).getContent();

                if(!pendingStrategies.contains(strategy)) pendingStrategies.add(strategy);
                if(this.equals(strategy.getOwner()) && !ownedStrategies.contains(strategy)) ownedStrategies.add(strategy);
            }
            newPairs.clear();

            writeACK();
            return null;
        };
    }


    /*****************************************
     *          RECEPTION FUNCTIONS          *
     *****************************************/

    @SuppressWarnings("unchecked")
    protected void receiveCollective() {
        int i = 0;
        while(i < team.size() - 1) {
            try {
                Message message = messages.take();
                if (message.getType() == COLLECTIVE_CONCERNED) {
                    List<List<Strategy>> content = ((ContentMessage<List<List<Strategy>>>) message).getContent();

                    List<Strategy> addingStrategies = content.get(0);
                    if (!addingStrategies.isEmpty()) {
                        addingStrategies.forEach(strategy -> moves.forEach(move -> {
                            if (strategy.isEligible(move)) {
                                newPairs.add(new StrategyPair(strategy, move));
                            }
                        }));
                    }

                    List<Strategy> withdrawingStrategies = content.get(1);
                    if (!withdrawingStrategies.isEmpty()) pendingStrategies.removeAll(withdrawingStrategies);
                }

                i++;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        getNotOwnedPendingCollectives().forEach(strategy -> moves.forEach(move -> {
            if (strategy.isEligible(move)) newPairs.add(new StrategyPair(strategy, move));
        }));
    }


    /***************************************
     *          SENDING FUNCTIONS          *
     ***************************************/

    /**
     * Sends the given message to the given receiver.
     * @param receiver the piece that can receive the message
     * @param message the message to send
     */
    protected void write(Piece receiver, Message message) {
        try {
            receiver.messages.put(message);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Sends the given message to all given receivers.
     * @param receivers all pieces that can receive the message
     * @param message the message to send
     */
    protected void write(List<Piece> receivers, Message message) {
        receivers.forEach(r -> write(r, message));
    }

    protected void writeACK() {
        try {
            team.getKing().acks.put(new Message(this, ACK));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
