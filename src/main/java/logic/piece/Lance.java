package logic.piece;

import java.util.List;

import javafx.geometry.Point2D;
import logic.move.DropUtilities;
import logic.move.ForwardUtilities;
import logic.move.Motion;
import logic.util.Team;

public final class Lance extends Promotable {

    public Lance(String letter, Point2D position, Team team) {
        super(letter, position, team, 4.30, 4.80, 6.30);
    }

    @Override
    public List<Motion> getForwardPositions(Point2D position, Point2D... toExclude) {
        return (isPromoted)
                ? ForwardUtilities.goldPositions(position, team, toExclude)
                : ForwardUtilities.lancePositions(position, team, toExclude);
    }

    @Override
    protected List<Motion> getDropPositions() {
        return DropUtilities.lancePositions(team);
    }

    @Override
    protected boolean shouldPromote(Point2D movePosition) {
        return isPromotablePosition(movePosition, 1);
    }
}
