package logic.piece;

import java.util.List;

import javafx.geometry.Point2D;
import logic.move.DropUtilities;
import logic.move.ForwardUtilities;
import logic.move.Motion;
import logic.util.Team;

public final class Bishop extends Promotable {

    public Bishop(String letter, Point2D position, Team team) {
        super(letter, position, team, 8.90, 11.10, 11.50);
    }

    @Override
    public List<Motion> getForwardPositions(Point2D position, Point2D... toExclude) {
        return (isPromoted)
                ? ForwardUtilities.promotedBishopPositions(position, team, toExclude)
                : ForwardUtilities.bishopPositions(position, team, toExclude);
    }

    @Override
    protected List<Motion> getDropPositions() {
        return DropUtilities.defaultPositions(team);
    }

    @Override
    protected boolean shouldPromote(Point2D movePosition) {
        return false;
    }
}
