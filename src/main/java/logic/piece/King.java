package logic.piece;

import static logic.message.MessageType.OPENING_CONCERNED;
import static logic.message.MessageType.OPENING_UNCONCERNED;
import static logic.message.MessageType.SEND_MOVE;
import static logic.message.MessageType.SEND_PAIR;
import static logic.message.MessageType.STRATEGY_CONCERNED;
import static logic.message.MessageType.STRATEGY_UNCONCERNED;
import static logic.strategy.StrategyType.rookPositions;
import control.ShogIAApp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import javafx.geometry.Point2D;
import logic.message.ContentMessage;
import logic.message.Message;
import logic.move.ForwardUtilities;
import logic.move.Motion;
import logic.move.Move;
import logic.strategy.Collective;
import logic.strategy.Strategy;
import logic.strategy.StrategyPair;
import logic.strategy.StrategyType;
import logic.strategy.passive.Castle;
import logic.strategy.passive.Passive;
import logic.strategy.passive.RookPosition;
import logic.util.Team;
import logic.util.ThreadPoolExecutorWithErrors;

public final class King extends Piece {

    private ThreadPoolExecutorWithErrors executor;
    private ShogIAApp app;
    private boolean openingsInitialized = false;
    public LinkedBlockingQueue<Message> acks = new LinkedBlockingQueue<>();

    public King(String letter, Point2D position, Team team) {
        super(letter, position, team, 1000, 1000);
    }

    public void init(ShogIAApp app) {
        this.app = app;
    }

    @Override
    public List<Motion> getForwardPositions(Point2D position, Point2D... toExclude) {
        return ForwardUtilities.kingPositions(position, team, toExclude);
    }

    @Override
    protected List<Motion> getDropPositions() {
        return null;
    }

    /**
     * Sets the executor with a fixed number of threads, here the number of pieces of the team.
     */
    public void setExecutor() {
        executor = new ThreadPoolExecutorWithErrors(20, 39, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<>());
    }

    /**
     * Executes all tasks present in the given list.
     * @param tasks the list of callable tasks
     */
    private void execute(List<Callable<Void>> tasks) {
        try {
            executor.setCorePoolSize(team.size());
            executor.invokeAll(tasks);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Shutdowns the executor.
     */
    public void shutdown() {
        executor.shutdownNow();
    }


    /***********************************
     *          THREADED PART          *
     ***********************************/

    /**
     * Executes all gotten threaded and callable tasks for distributed modeling.
     */
    public void buildDecision() {
        // CALCUL
        System.out.println("\n");
        if (team.lastFallen != null) execute(prepareRemovingFallen());
        if (!openingsInitialized) execute(prepareOpenings());
        execute(prepareMoveComputation());
        execute(prepareStrategyMatching());

        // DECISION
        execute(prepareNewCollectiveSending());
        execute(prepareNewCollectiveReceive());

        // FINAL
        execute(prepareSelection());
    }


    /***************************************
     *          PREPARATION TASKS          *
     ***************************************/

    private List<Callable<Void>> prepareRemovingFallen() {
        return team.getPieces().stream().map(Piece::removingFallenPhase).collect(Collectors.toList());
    }

    private List<Callable<Void>> prepareOpenings() {
        return team.getPieces().stream().map(Piece::openingPreparationPhase).collect(Collectors.toList());
    }

    private List<Callable<Void>> prepareMoveComputation() {
        return team.getPieces().stream().map(Piece::moveComputationPhase).collect(Collectors.toList());
    }

    private List<Callable<Void>> prepareStrategyMatching() {
        return team.getPieces().stream().map(Piece::strategyMatchingPhase).collect(Collectors.toList());
    }

    private List<Callable<Void>> prepareNewCollectiveSending() {
        return team.getPieces().stream().map(Piece::newCollectiveSendingPhase).collect(Collectors.toList());
    }

    private List<Callable<Void>> prepareNewCollectiveReceive() {
        return team.getPieces().stream().map(Piece::newCollectiveReceivePhase).collect(Collectors.toList());
    }

    private List<Callable<Void>> prepareSelection() {
        return team.getPieces().stream().map(Piece::moveSendingBestStrategyPair).collect(Collectors.toList());
    }


    /**************************************
     *          CONTENT OF TASKS          *
     **************************************/

    @Override
    protected Callable<Void> removingFallenPhase() {
        return () -> {
            System.out.println("removingFallenPhase start");
            removingFallen();

            receiveAcknowledgment();
            team.lastFallen = null;
            System.out.println("removingFallenPhase end");
            return null;
        };
    }

    @Override
    protected Callable<Void> openingPreparationPhase() {
        return () -> {
            System.out.println("openingPreparationPhase start");
            openingsInitialized = true;

            StrategyType rookType = rookPositions().get(new Random().nextInt(rookPositions().size()));
            RookPosition rookPosition = (RookPosition) Strategy.instantiate(rookType, this);

            List<StrategyType> matchingCastles = rookPosition.getMatchingCastles();
            StrategyType castleType = matchingCastles.get(new Random().nextInt(matchingCastles.size()));
            Castle castle = (Castle) Strategy.instantiate(castleType, this);

            ownedStrategies.add(rookPosition);
            ownedStrategies.add(castle);
            pendingStrategies.add(castle);

            List<Piece> toSendRookPosition = rookPosition.getAllies();
            List<Piece> toSendCastle = castle.getAllies();

            team.getAllExcepted(this).forEach(piece -> {
                List<Strategy> strategiesToSend = new ArrayList<>();

                if (toSendRookPosition.contains(piece))
                    strategiesToSend.add(rookPosition);
                if (toSendCastle.contains(piece))
                    strategiesToSend.add(castle);

                if (strategiesToSend.isEmpty())
                    write(piece, new Message(this, OPENING_UNCONCERNED));
                else
                    write(piece, new ContentMessage<>(this, OPENING_CONCERNED, strategiesToSend));
            });
            receiveAcknowledgment();
            System.out.println("openingPreparationPhase end");
            return null;
        };
    }

    @Override
    protected Callable<Void> moveComputationPhase() {
        return () -> {
            System.out.println("moveComputationPhase start");
            threatComputation();
            moveComputation();

            receiveAcknowledgment();
            System.out.println("moveComputationPhase end");
            return null;
        };
    }

    @Override
    protected Callable<Void> strategyMatchingPhase() {
        return () -> {
            System.out.println("strategyMatchingPhase start");
            strategyMatching();

            receiveAcknowledgment();
            System.out.println("strategyMatchingPhase end");
            return null;
        };
    }

    @Override
    protected Callable<Void> newCollectiveSendingPhase() {
        return () -> {
            System.out.println("newCollectiveSendingPhase start");
            collectiveSending();

            receiveAcknowledgment();
            System.out.println("newCollectiveSendingPhase end");
            return null;
        };
    }

    @Override
    protected Callable<Void> newCollectiveReceivePhase() {
        return () -> {
            System.out.println("newCollectiveReceivePhase start");
            receiveCollective();

            receiveAcknowledgment();
            System.out.println("newCollectiveReceivePhase end");
            return null;
        };
    }

    @Override
    protected Callable<Void> moveSendingBestStrategyPair() {
        return () -> {
            System.out.println("moveSendingBestStrategyPair start");
            List<StrategyPair> sortedPairsToChoose = new ArrayList<>();
            if(newPairs.isEmpty() && !moves.isEmpty()) {
                Collections.sort(moves);
                sortedPairsToChoose.add(new StrategyPair(null, moves.get(0)));
            } else if(!newPairs.isEmpty()) {
                Collections.sort(newPairs);
                sortedPairsToChoose.add(newPairs.get(0));
            }
            sortedPairsToChoose.addAll(receivePairsLoop());
            Collections.sort(sortedPairsToChoose);

            sortedPairsToChoose.forEach(pair -> {
                Optional<Strategy> optStrategy = Optional.ofNullable(pair.first());
                Move move = pair.second();
                Piece actor = move.getActor();

                System.out.println("------------------------");
                optStrategy.ifPresent(strategy -> {
                    Piece owner = strategy.getOwner();

                    System.out.println("Strategy name: " + strategy.getClass().getSimpleName());
                    System.out.println("Strategy score: " + strategy.getScore());
                    System.out.println("Proposed by " + owner.getClass().getSimpleName() + " " + owner.id);
                });
                System.out.println("Move score: " + move.getScore());
                System.out.println("Can be played by " + actor.getClass().getSimpleName() + " " + actor.id);
            });

            StrategyPair bestPair = sortedPairsToChoose.get(0);
            Strategy strategy = bestPair.first();
            Move move = bestPair.second();

            System.out.println("\n------------------------");
            if (strategy != null) {
                System.out.println("Chosen strategy: " + strategy.getClass().getSimpleName() +
                    ", proposed by " + strategy.getOwner().getClass().getSimpleName() + " " + strategy.getOwner().id +
                    ", played by " + move.getActor().getClass().getSimpleName() + " " + move.getActor().id + "\n"
                );

                if (strategy instanceof Passive) strategy.progress(move);
                else strategy.progress();
                
                List<Piece> toSend = new ArrayList<>();
                if(strategy instanceof Collective) toSend.addAll(((Collective) strategy).getAllies());
                if(!toSend.contains(strategy.getOwner())) toSend.add(strategy.getOwner());

                if (this.equals(strategy.getOwner()) && !ownedStrategies.contains(strategy)) ownedStrategies.add(strategy);
                if(toSend.contains(this) && !pendingStrategies.contains(strategy)) pendingStrategies.add(strategy);

                team.getAllExcepted(this).forEach(piece -> {
                    if (toSend.contains(piece))
                        write(piece, new ContentMessage<>(this, STRATEGY_CONCERNED, strategy));
                    else
                        write(piece, new Message(this, STRATEGY_UNCONCERNED));
                });

                team.statistics.merge(strategy.getType().name(), 1, Integer::sum);
            } else {
                System.out.println("Chosen strategy: BestMove, played by " + move.getActor().getClass().getSimpleName() + "\n");
                write(team.getAllExcepted(this), new Message(this, STRATEGY_UNCONCERNED));

                team.statistics.merge("JUST MOVE", 1, Integer::sum);
            }

            newPairs.clear();
            receiveAcknowledgment();

            writeDecision(new ContentMessage<>(this, SEND_MOVE, move));
            System.out.println("moveSendingBestStrategyPair end");
            return null;
        };
    }


    /*****************************************
     *          RECEPTION FUNCTIONS          *
     *****************************************/

    private void receiveAcknowledgment() {
        int i = 0;
        while(i < team.size() - 1) {
            try {
                acks.take();
                i++;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @SuppressWarnings("unchecked")
    private List<StrategyPair> receivePairsLoop() {
        int i = 0;
        List<StrategyPair> toChoose = new ArrayList<>();
        while (i < team.size() - 1) {
            try {
                Message message = messages.take();
                if(message.getType() == SEND_PAIR)
                    toChoose.add(((ContentMessage<StrategyPair>) message).getContent());

                i++;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return toChoose;
    }


    /***************************************
     *          SENDING FUNCTIONS          *
     ***************************************/

    /**
     * Sends the given message to main app, to notify team decision.
     * @param message the message to send
     */
    private void writeDecision(ContentMessage<Move> message) {
        try {
            app.messages.put(message);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
