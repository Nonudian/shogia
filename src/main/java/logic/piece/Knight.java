package logic.piece;

import java.util.List;

import javafx.geometry.Point2D;
import logic.move.DropUtilities;
import logic.move.ForwardUtilities;
import logic.move.Motion;
import logic.util.Team;

public final class Knight extends Promotable {

    public Knight(String letter, Point2D position, Team team) {
        super(letter, position, team, 4.50, 5.10, 6.40);
    }

    @Override
    public List<Motion> getForwardPositions(Point2D position, Point2D... toExclude) {
        return (isPromoted)
                ? ForwardUtilities.goldPositions(position, team, toExclude)
                : ForwardUtilities.knightPositions(position, team, toExclude);
    }

    @Override
    protected List<Motion> getDropPositions() {
        return DropUtilities.knightPositions(team);
    }

    @Override
    protected boolean shouldPromote(Point2D movePosition) {
        return isPromotablePosition(movePosition, 2);
    }
}
