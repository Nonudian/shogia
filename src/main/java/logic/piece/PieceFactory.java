package logic.piece;

import static logic.constant.Consts.BISHOP;
import static logic.constant.Consts.GOLD;
import static logic.constant.Consts.INITIAL_LAYOUT;
import static logic.constant.Consts.KING;
import static logic.constant.Consts.KNIGHT;
import static logic.constant.Consts.LANCE;
import static logic.constant.Consts.PAWN;
import static logic.constant.Consts.ROOK;
import static logic.constant.Consts.SILVER;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javafx.geometry.Point2D;
import logic.util.Team;

public final class PieceFactory {

    private PieceFactory() {}

    /**
     * @param team the team that will be linked to pieces
     * @return a list of built pieces
     */
    public static List<Piece> buildPieces(Team team) {
        List<Piece> allPieces = new ArrayList<>();
        for (int y = 0; y < 3; y++) {
            for (int x = 0; x < 9; x++) {
                Optional.ofNullable(buildPiece(INITIAL_LAYOUT[y][x], computePosition(x, y, team.origin, team.orientationFactor), team))
                        .ifPresent(allPieces::add);
            }
        }
        return allPieces;
    }

    /**
     * @param x the x coordinate
     * @param y the y coordinate
     * @param origin the origin value defined for each team
     * @param orientationFactor the orientation value defined for each team
     * @return the computation of real position on board
     */
    private static Point2D computePosition(int x, int y, int origin, int orientationFactor) {
        return new Point2D(origin + orientationFactor * x, origin + orientationFactor * y);
    }

    /**
     * @param letter the piece letter
     * @param position the initial piece position
     * @param team the piece team
     * @return a built piece with all params
     */
    private static Piece buildPiece(String letter, Point2D position, Team team) {
        if (letter.equals(LANCE)) return new Lance(letter, position, team);
        else if (letter.equals(KNIGHT)) return new Knight(letter, position, team);
        else if (letter.equals(SILVER)) return new Silver(letter, position, team);
        else if (letter.equals(GOLD)) return new Gold(letter, position, team);
        else if (letter.equals(KING)) return new King(letter, position, team);
        else if (letter.equals(ROOK)) return new Rook(letter, position, team);
        else if (letter.equals(BISHOP)) return new Bishop(letter, position, team);
        else if (letter.equals(PAWN)) return new Pawn(letter, position, team);
        else return null;
    }
}
