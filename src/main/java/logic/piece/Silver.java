package logic.piece;

import java.util.List;

import javafx.geometry.Point2D;
import logic.move.DropUtilities;
import logic.move.ForwardUtilities;
import logic.move.Motion;
import logic.util.Team;

public final class Silver extends Promotable {

    public Silver(String letter, Point2D position, Team team) {
        super(letter, position, team, 6.40, 7.20, 6.70);
    }

    @Override
    public List<Motion> getForwardPositions(Point2D position, Point2D... toExclude) {
        return (isPromoted)
                ? ForwardUtilities.goldPositions(position, team, toExclude)
                : ForwardUtilities.silverPositions(position, team, toExclude);
    }

    @Override
    protected List<Motion> getDropPositions() {
        return DropUtilities.defaultPositions(team);
    }

    @Override
    protected boolean shouldPromote(Point2D movePosition) {
        return false;
    }
}
