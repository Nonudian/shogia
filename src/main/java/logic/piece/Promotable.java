package logic.piece;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javafx.geometry.Point2D;
import logic.move.Move;
import logic.util.Team;

public abstract class Promotable extends Piece {

    protected boolean isPromoted = false;
    protected final double promotionValuation;

    public Promotable(
            String letter,
            Point2D position,
            Team team,
            double basicValuation,
            double inHandValuation,
            double promotionValuation) {
        super(letter, position, team, basicValuation, inHandValuation);
        this.promotionValuation = promotionValuation;
    }

    public double getPromotionValuation() {
        return promotionValuation;
    }

    /**
     * Promotes this promotable piece.
     */
    public void bePromoted() {
        isPromoted = true;
        valuation = promotionValuation;
    }

    /**
     * Demotes this promotable piece.
     */
    public void beDemoted() {
        isPromoted = false;
    }

    /**
     * @return a boolean that describes the current promoted status of this piece
     */
    public boolean isPromoted() {
        return isPromoted;
    }

    /**
     * @param position the given position to analyse
     * @param deltaY the y row and less
     * @return a boolean that indicates if the given position is promotable.
     */
    protected boolean isPromotablePosition(Point2D position, int deltaY) {
        if(position == null) return false;
        return team.getOpposite().origin + team.getOpposite().orientationFactor * position.getY() < deltaY;
    }

    /**
     * To avoid return a boolean this three values (could promote, should promote, and nothing),
     * that uses an Optional for possible promotion, and two booleans for the required promotion
     * (null, true, or false).
     *
     * @param movePosition the given position to analyse
     * @return a boolean (Optional.of()) if there is possible promotion, otherwise null (Optional.empty())
     */
    public Optional<Boolean> couldPromote(Point2D movePosition) {
        return isPromotablePosition(position, 3) || isPromotablePosition(movePosition, 3)
                ? Optional.of(shouldPromote(movePosition))
                : Optional.empty();
    }

    /**
     * @param movePosition the given position to analyse
     * @return a boolean that checks the mandatory characteristic of the possible promotion
     */
    protected abstract boolean shouldPromote(Point2D movePosition);

    /**
     * Override version of this parent function.
     * Updates moves, but checks the possibility of promotion in addition.
     */
    @Override
    protected void moveComputation() {
        super.moveComputation();
        if (!isPromoted && position != null) {
            List<Move> additional = new ArrayList<>();
            moves.forEach(move -> couldPromote(move.getFinalPosition())
                    .ifPresent(condition -> {
                        if(!condition) additional.add(new Move(move));
                        move.withPromotion();
                    }));
            moves.addAll(additional);
        }
    }
}
