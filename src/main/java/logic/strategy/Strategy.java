package logic.strategy;

import logic.move.Move;
import logic.piece.King;
import logic.piece.Piece;
import logic.strategy.proactive.attack.Bait;
import logic.strategy.proactive.attack.Check;
import logic.strategy.proactive.attack.Dangling;
import logic.strategy.proactive.attack.Fork;
import logic.strategy.proactive.attack.Mate;
import logic.strategy.proactive.attack.Pin;
import logic.strategy.passive.Castle;
import logic.strategy.passive.RookPosition;
import logic.strategy.proactive.defense.Flee;
import logic.strategy.proactive.defense.KingProtection;
import logic.util.Scorable;

import static logic.strategy.Strategy.StrategyState.*;
import static logic.strategy.Strategy.StrategyState.ABORTED;

public abstract class Strategy implements Scorable {

    protected StrategyType type;
    protected StrategyState state = PENDING;
    protected Piece owner;

    protected Strategy(StrategyType type, Piece owner) {
        this.type = type;
        this.owner = owner;
    }

    @Override
    public double getScore() {
        return 10;
    }

    public StrategyType getType() {
        return type;
    }

    public Piece getOwner() {
        return owner;
    }

    public boolean isTerminated() {
        return state == ABORTED || state == COMPLETED;
    }

    /**
     * Checks any possible termination, and sets the state of this strategy with ABORTED or COMPLETED,
     * otherwise do nothing.
     */
    public void checkTermination() {
        if (shouldComplete()) { complete(); return; }
        if (shouldAbort()) { abort(); }
    }

    protected void abort() {
        state = ABORTED;
    }
    protected void complete() {
        state = COMPLETED;
    }

    /**
     * Checks the eligibility (more precisely the possibility, according to the current game situation)
     * of the given move to apply the strategy.
     * @param move the move that's possibly matching with this strategy
     * @return a boolean that indicates the result of the eligibility condition
     */
    public abstract boolean isEligible(Move move);

    /**
     * Makes this strategy progress at each turn.
     * @param args varargs that could be empty or fulfil, according the children behaviour
     */
    public abstract void progress(Move... args);

    protected abstract boolean shouldComplete();
    protected abstract boolean shouldAbort();

    public static Strategy instantiate(StrategyType type, Piece owner) {
        switch (type) {
            case LEFT_GOLD_FORTRESS: return new Castle.LeftGoldFortress((King) owner);
            case RIGHT_GOLD_FORTRESS: return new Castle.RightGoldFortress((King) owner);
            case LEFT_MINO: return new Castle.LeftMino((King) owner);
            case RIGHT_MINO: return new Castle.RightMino((King) owner);
            case LEFT_SILVER_CROWN: return new Castle.LeftSilverCrown((King) owner);
            case RIGHT_SILVER_CROWN: return new Castle.RightSilverCrown((King) owner);
            case LEFT_BEAR_HOLE: return new Castle.LeftBearHole((King) owner);
            case RIGHT_BEAR_HOLE: return new Castle.RightBearHole((King) owner);
            case TRUE_STATIC_ROOK: return new RookPosition.TrueStaticRook((King) owner);
            case THIRD_RANGING_ROOK: return new RookPosition.ThirdRangingRook((King) owner);
            case BAIT: return new Bait(owner);
            case DANGLING: return new Dangling(owner);
            case FORK: return new Fork(owner);
            case PIN: return new Pin(owner);
            case FLEE: return new Flee(owner);
            case KING_PROTECTION: return new KingProtection(owner);
            case CHECK: return new Check(owner);
            case MATE: return new Mate(owner);
            default: return null;
        }
    }

    protected enum StrategyState {
        PENDING,
        COMPLETED,
        ABORTED
    }
}
