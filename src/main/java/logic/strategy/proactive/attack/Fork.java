package logic.strategy.proactive.attack;

import javafx.geometry.Point2D;
import logic.move.Move;
import logic.piece.Piece;
import logic.strategy.proactive.Proactive;

import java.util.ArrayList;
import java.util.List;
import static logic.strategy.StrategyType.FORK;

public class Fork extends Proactive {

    private List<Piece> forked;
    private Point2D forkerPosition;

    public Fork(Piece owner) {
        super(FORK, owner, 2);
    }

    @Override
    protected boolean lastCondToCheck() {
        return forked.stream().anyMatch(Piece::inHand);
    }

    @Override
    protected boolean shouldAbort() {
        boolean check = false;
        // TODO: always the same... check if we not applied the last move... ?
        if(currentSteps == 1) check = !owner.getPosition().equals(forkerPosition);
        return super.shouldAbort() || check;
    }

    @Override
    public boolean isEligible(Move move) {
        switch(currentSteps) {
            case 0:
                if(move.isRisky()) return false;
                forkerPosition = move.getFinalPosition();
                forked = new ArrayList<>(move.getPotentialTargets().values());
                return forked.size() >= 2;
            case 1:
                return move.hasTarget() && forked.contains(move.getTarget());
            default:
                return false;
        }
    }

    @Override
    public double getScore() {
        return super.getScore() + forked.stream().map(Piece::getValuation).min(Double::compare).orElse(0.) - owner.getValuation();
    }
}
