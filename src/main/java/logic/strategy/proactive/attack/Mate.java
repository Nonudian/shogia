package logic.strategy.proactive.attack;

import javafx.geometry.Point2D;
import logic.move.Move;
import logic.piece.King;
import logic.piece.Piece;
import logic.strategy.proactive.Proactive;

import static logic.strategy.StrategyType.MATE;

public class Mate extends Proactive {

    King king;
    Point2D kingPosition;

    public Mate(Piece owner) {
        super(MATE, owner, 1);
        king = owner.getTeam().getOpposite().getKing();
        kingPosition = king.getPosition();
    }

    @Override
    public double getScore() {
        return super.getScore() + 2*king.getValuation();
    }

    @Override
    protected boolean lastCondToCheck() {
        return true;
    }

    @Override
    public boolean isEligible(Move move) {
        if (currentSteps == 0) return move.hasTarget() && move.getTarget().equals(king);
        return false;
    }
}
