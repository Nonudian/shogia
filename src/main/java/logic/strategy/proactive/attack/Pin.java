package logic.strategy.proactive.attack;

import javafx.geometry.Point2D;
import logic.move.Direction;
import logic.move.Move;
import logic.piece.Bishop;
import logic.piece.Lance;
import logic.piece.Piece;
import logic.piece.Rook;
import logic.strategy.proactive.Proactive;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import static logic.strategy.StrategyType.PIN;

public class Pin extends Proactive {

    private Map<Direction, Piece> pinned;
    private final Map<Direction, Piece> targets = new HashMap<>();
    private Point2D pinnerPosition;

    public Pin(Piece owner) {
        super(PIN, owner, 2);
    }

    @Override
    protected boolean lastCondToCheck() {
        AtomicBoolean inhand = new AtomicBoolean(false);
        pinned.forEach((dir, piece) -> { if(piece.inHand()) inhand.set(true); });
        targets.forEach((dir, piece) -> { if(piece.inHand()) inhand.set(true); });
        return inhand.get();
    }

    @Override
    protected boolean shouldAbort() {
        boolean check = false;
        // TODO: always the same... check if we not applied the last move... ?
        if(currentSteps == 1) check = !owner.getPosition().equals(pinnerPosition);
        return super.shouldAbort() || check;
    }

    @Override
    public boolean isEligible(Move move) {
        switch(currentSteps) {
            case 0:
                if(!(owner instanceof Lance || owner instanceof Rook || owner instanceof Bishop)) return false;
                if(move.isRisky()) return false;

                pinnerPosition = move.getFinalPosition();

                pinned = move.getPotentialTargets();
                if(pinned.isEmpty()) return false;

                pinned.forEach((dir, piece) -> owner.getForwardPositions(piece.getPosition(), owner.getPosition())
                    .stream()
                    .filter(motion -> motion.getDirection() == dir)
                    .filter(motion -> owner.getTeam().isPresentEnemy(motion.getFinalPosition()))
                    .map(motion -> owner.getTeam().getOpposite().getPieceByPosition(motion.getFinalPosition()).get())
                    .findFirst()
                    .ifPresent(target -> {
                        if(target.getValuation() > piece.getValuation()) targets.put(dir, target);
                    }));
                if(targets.isEmpty()) return false;
            case 1:
                return move.hasTarget() && (pinned.containsValue(move.getTarget()) || targets.containsValue(move.getTarget()));
            default:
                return false;
        }
    }

    @Override
    public double getScore() {
        return super.getScore() + pinned.values().stream().map(Piece::getValuation).min(Double::compare).orElse(0.)
            + targets.values().stream().map(Piece::getValuation).min(Double::compare).orElse(0.)
            - owner.getValuation();
    }
}
