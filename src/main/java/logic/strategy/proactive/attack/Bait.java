package logic.strategy.proactive.attack;

import java.util.ArrayList;
import javafx.geometry.Point2D;
import logic.move.Move;
import logic.piece.King;
import logic.piece.Piece;
import logic.strategy.proactive.Proactive;
import logic.strategy.Collective;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.function.Predicate.not;
import static java.util.function.Predicate.isEqual;
import static logic.strategy.StrategyType.BAIT;

public class Bait extends Proactive implements Collective {

    private List<Piece> avengers = new ArrayList<>();
    private List<Piece> oldAvengers = new ArrayList<>();
    private List<Piece> baited;
    private Point2D baitedPosition;

    public Bait(Piece owner) {
        super(BAIT, owner, 2);
        setAllies();
    }

    // Les checks se font en début de tour par rapport à ce qui a été décidé au tour d'avant
    @Override
    protected boolean lastCondToCheck() {
        return baited.stream().anyMatch(Piece::inHand);
    }

    @Override
    protected boolean shouldAbort() {
        boolean check = false;
        // TODO: always the same... check if we not applied the last move... ?
        if(currentSteps == 1) check = !owner.getPosition().equals(baitedPosition);
        return super.shouldAbort() || check || avengers.isEmpty();
    }

    // Les phases suivantes se font si la stratégie est toujours en pending.
    @Override
    public boolean isEligible(Move move) {
        switch(currentSteps) {
            case 0:
                if(!move.isRisky() || move.getActor() instanceof King) return false;
                baited = move.getThreats();
                baitedPosition = move.getFinalPosition();
                return baited.stream().anyMatch(threat -> threat.getValuation() <= owner.getValuation());
            case 1:
                return owner.inHand() && move.hasTarget() && baited.contains(move.getTarget());
            default:
                return false;
        }
    }

    @Override
    public void setAllies() {
        oldAvengers = avengers;
        avengers = owner.getTeam().actives.stream()
            .filter(not(isEqual(owner)))
            .map(Piece::getMoves)
            .flatMap(Collection::stream)
            .filter(m -> m.getFinalPosition().equals(baitedPosition))
            .map(Move::getActor)
            .distinct()
            .collect(Collectors.toList());
    }

    @Override
    public double getScore() {
        return super.getScore() + avengers.size()
            - baited.size()
            + baited.stream().map(Piece::getValuation).min(Double::compare).orElse(0.)
            - owner.getValuation();
    }

    @Override
    public List<Piece> getAllies() {
        return avengers;
    }

    @Override
    public List<Piece> getOldAllies() {
        return oldAvengers;
    }

    @Override
    public void removeFallen(Piece fallen) {
        this.avengers.removeIf(fallen::equals);
        this.oldAvengers.removeIf(fallen::equals);
    }
}
