package logic.strategy.proactive.attack;

import javafx.geometry.Point2D;
import logic.move.Move;
import logic.piece.King;
import logic.piece.Piece;
import logic.strategy.proactive.Proactive;

import static logic.strategy.StrategyType.CHECK;

public class Check extends Proactive {

    King king;
    Point2D kingPosition;

    public Check(Piece owner) {
        super(CHECK, owner, 1);
        king = owner.getTeam().getOpposite().getKing();
        kingPosition = king.getPosition();
    }

    @Override
    public double getScore() {
        return super.getScore() + king.getValuation();
    }

    @Override
    protected boolean lastCondToCheck() {
        return !king.getPosition().equals(kingPosition);
    }

    @Override
    public boolean isEligible(Move move) {
        if (currentSteps == 0) return !move.isRisky() && move.hasPotentialTargets() && move.getPotentialTargets().containsValue(king);
        return false;
    }
}
