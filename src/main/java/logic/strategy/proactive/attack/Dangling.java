package logic.strategy.proactive.attack;

import javafx.geometry.Point2D;
import logic.move.Move;
import logic.piece.Piece;
import logic.piece.Promotable;
import logic.strategy.proactive.Proactive;

import static logic.move.Move.MoveState.DROP;
import static logic.move.Move.MoveState.PROMOTING_MOVE;
import static logic.move.Move.MoveState.CAPTURING_AND_PROMOTING_MOVE;
import static logic.strategy.StrategyType.DANGLING;

public class Dangling extends Proactive {

    private Point2D danglerPosition;

    public Dangling(Piece owner) {
        super(DANGLING, owner, 2);
    }

    @Override
    protected boolean lastCondToCheck() {
        return ((Promotable)owner).isPromoted();
    }

    @Override
    protected boolean shouldAbort() {
        boolean check = false;
        // TODO: always the same... check if we not applied the last move... ?
        if(currentSteps == 1) check = !owner.getPosition().equals(danglerPosition);
        return super.shouldAbort() || check;
    }

    @Override
    public boolean isEligible(Move move) {
        switch(currentSteps) {
            case 0:
                if(!(owner instanceof Promotable
                    && move.getState() == DROP
                    && !move.isRisky())) return false;
                danglerPosition = move.getFinalPosition();
                return owner.getForwardPositions(danglerPosition)
                    .stream()
                    .anyMatch(motion -> ((Promotable)owner).couldPromote(motion.getFinalPosition()).isPresent());
            case 1:
                return !move.isRisky() && (move.getState() == PROMOTING_MOVE || move.getState() == CAPTURING_AND_PROMOTING_MOVE);
            default:
                return false;
        }
    }

    @Override
    public double getScore() {
        return super.getScore() + ((Promotable)owner).getPromotionValuation() - owner.getValuation();
    }
}
