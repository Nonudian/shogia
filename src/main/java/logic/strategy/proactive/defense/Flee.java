package logic.strategy.proactive.defense;

import logic.move.Move;
import logic.piece.Piece;
import logic.strategy.proactive.Proactive;

import static logic.strategy.StrategyType.FLEE;

public class Flee extends Proactive {
    public Flee(Piece owner) {
        super(FLEE, owner, 1);
    }

    @Override
    protected boolean lastCondToCheck() {
        return !owner.inHand();
    }

    @Override
    public boolean isEligible(Move move) {
        if (currentSteps == 0) return owner.isThreatened() && !move.isRisky();
        return false;
    }

    @Override
    public double getScore() {
        return super.getScore() + owner.getValuation();
    }
}