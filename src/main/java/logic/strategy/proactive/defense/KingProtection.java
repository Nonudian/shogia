package logic.strategy.proactive.defense;

import logic.move.Move;
import logic.piece.King;
import logic.piece.Piece;
import logic.strategy.proactive.Proactive;

import java.util.List;
import java.util.Optional;

import static logic.strategy.StrategyType.KING_PROTECTION;

public class KingProtection extends Proactive {

    King king;
    Piece protector;
    Piece threat;

    public KingProtection(Piece owner) {
        super(KING_PROTECTION, owner, 1);
        king = owner.getTeam().getKing();
    }

    @Override
    public boolean isEligible(Move move) {
        if (currentSteps == 0) {
            if(move.getActor() instanceof King) return false; // King can't protect itself

            Optional<List<Piece>> kingThreats = king.getThreats();
            if(kingThreats.isEmpty()) return false; // The king isn't in danger

            List<Piece> threats = kingThreats.get();
            if(threats.size() > 1) return false; // Can't save the king if he is checkmated

            protector = move.getActor();
            threat = threats.get(0);
            boolean exterminate = move.hasTarget() && move.getTarget().equals(threat);
            // TODO: check if our move stop the king from getting threatened
            //boolean sacrifice = move.isRisky() && move.getThreats().contains(threat);
            return exterminate;// || sacrifice;
        }
        return false;
    }

    @Override
    public double getScore() {
        return super.getScore() + 2*king.getValuation();
    }

    @Override
    protected boolean lastCondToCheck() {
        return threat.inHand() || protector.inHand();
    }
}
