package logic.strategy.proactive;

import logic.move.Move;
import logic.piece.Piece;
import logic.strategy.Strategy;
import logic.strategy.StrategyType;

public abstract class Proactive extends Strategy {

    protected int currentSteps = 0;
    protected int maxSteps;

    public Proactive(StrategyType type, Piece owner, int maxSteps) {
        super(type, owner);
        this.maxSteps = maxSteps;
    }

    protected abstract boolean lastCondToCheck();

    @Override
    protected boolean shouldComplete() {
        return lastStepAlreadyPlayed() && lastCondToCheck();
    }

    @Override
    protected boolean shouldAbort() {
        if(lastStepAlreadyPlayed()) return !lastCondToCheck();
        return false;
    }

    @Override
    public void progress(Move... args) {
        currentSteps++;
    }

    protected boolean lastStepAlreadyPlayed() {
        return currentSteps == maxSteps;
    }
}
