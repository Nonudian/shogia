package logic.strategy;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum StrategyType {

    LEFT_GOLD_FORTRESS,
    RIGHT_GOLD_FORTRESS,
    LEFT_MINO,
    RIGHT_MINO,
    LEFT_SILVER_CROWN,
    RIGHT_SILVER_CROWN,
    LEFT_BEAR_HOLE,
    RIGHT_BEAR_HOLE,
    TRUE_STATIC_ROOK,
    THIRD_RANGING_ROOK,
    BAIT,
    DANGLING,
    FORK,
    PIN,
    FLEE,
    KING_PROTECTION,
    CHECK,
    MATE;

    public static List<StrategyType> staticRooks() {
        return Collections.singletonList(TRUE_STATIC_ROOK);
    }

    public static List<StrategyType> rangingRooks() {
        return Collections.singletonList(THIRD_RANGING_ROOK);
    }

    public static List<StrategyType> rookPositions() {
        return Stream.of(staticRooks(), rangingRooks())
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }

    public static List<StrategyType> defenses() {
        return Arrays.asList(FLEE, KING_PROTECTION);
    }

    public static List<StrategyType> attacks() {
        return Arrays.asList(BAIT, DANGLING, FORK, PIN, CHECK, MATE);
    }

    public static List<StrategyType> proactives() {
        return Stream.of(attacks(), defenses())
            .flatMap(Collection::stream)
            .collect(Collectors.toList());
    }
}
