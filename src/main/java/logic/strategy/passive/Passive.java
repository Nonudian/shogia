package logic.strategy.passive;

import static java.util.function.Predicate.isEqual;
import static java.util.function.Predicate.not;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import javafx.geometry.Point2D;
import logic.move.Motion;
import logic.move.Move;
import logic.piece.King;
import logic.piece.Piece;
import logic.strategy.Collective;
import logic.strategy.Strategy;
import logic.strategy.StrategyType;

public abstract class Passive extends Strategy implements Collective {

    protected final Map<Piece, Point2D> finalPositions = new HashMap<>();
    protected final Map<Point2D, Integer> distances = new HashMap<>();
    protected List<Piece> allies = new ArrayList<>();
    protected List<Piece> oldAllies = new ArrayList<>();

    protected Passive(StrategyType type, King owner, Map<Point2D, Point2D> positions) {
        super(type, owner);
        positions.forEach((end, start) -> {
            Point2D startPosition = owner.getTeam().getTruePosition(start);
            Point2D endPosition = owner.getTeam().getTruePosition(end);
            Piece participant = owner.getTeam().getPieceByPosition(startPosition).get();

            finalPositions.put(participant, endPosition);
            distances.put(endPosition, computeDistance(endPosition, startPosition));
        });
        setAllies();
    }

    private int computeDistance(Point2D end, Point2D start) {
        return (int) Math.max(
                Math.abs(end.getX() - start.getX()),
                Math.abs(end.getY() - start.getY()));
    }

    @Override
    public boolean isEligible(Move move) {
        Piece actor = move.getActor();
        if (!finalPositions.containsKey(actor)) return false;

        Point2D finalPosition = finalPositions.get(actor);
        int oldDistance = distances.get(finalPosition);
        int newDistance = computeDistance(finalPosition, move.getFinalPosition());

        return newDistance < oldDistance;
    }

    @Override
    public void progress(Move... args) {
        Move move = args[0];
        Piece actor = move.getActor();

        Point2D trueFinalPosition = finalPositions.get(actor);
        Point2D moveFinalPosition = move.getFinalPosition();

        if (trueFinalPosition.equals(moveFinalPosition)) {
            finalPositions.remove(actor);
            distances.remove(moveFinalPosition);
            return;
        }
        distances.put(trueFinalPosition, computeDistance(trueFinalPosition, moveFinalPosition));
    }

    @Override
    protected boolean shouldComplete() {
        return finalPositions.isEmpty();
    }

    @Override
    protected boolean shouldAbort() {
        return allies.isEmpty() && !finalPositions.containsKey(owner);
    }

    @Override
    public void setAllies() {
        oldAllies = allies;
        allies = owner.getTeam().actives.stream()
            .filter(not(isEqual(owner)))
            .filter(finalPositions::containsKey)
            .collect(Collectors.toList());
    }

    @Override
    public List<Piece> getAllies() {
        return allies;
    }

    @Override
    public List<Piece> getOldAllies() {
        return oldAllies;
    }

    @Override
    public void removeFallen(Piece fallen) {
        this.allies.removeIf(fallen::equals);
        this.oldAllies.removeIf(fallen::equals);
    }

    public int leftoverMoveCounter(Move initialMove) {
        Piece actor = initialMove.getActor();
        Point2D trueFinalPosition = finalPositions.get(actor);

        return computePathfinding(initialMove.getFinalPosition(), 0, actor, trueFinalPosition, new ArrayList<>());
    }

    private int computePathfinding(Point2D currentPosition, int moveCounter, Piece actor, Point2D trueFinalPosition, List<Point2D> alreadyChecked) {
        Optional<Point2D> possibleNextPosition = actor.getForwardPositions(currentPosition, actor.getPosition())
                .stream()
                .map(Motion::getFinalPosition)
                .filter(not(alreadyChecked::contains))
                .min(Comparator.comparing(position -> computeDistance(trueFinalPosition, position)));

        if (possibleNextPosition.isEmpty()) return moveCounter;
        moveCounter++;

        Point2D nextPosition = possibleNextPosition.get();
        if (nextPosition.equals(trueFinalPosition)) return moveCounter;

        alreadyChecked.add(nextPosition);
        return computePathfinding(nextPosition, moveCounter, actor, trueFinalPosition, alreadyChecked);
    }
}
