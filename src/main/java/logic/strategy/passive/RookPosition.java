package logic.strategy.passive;

import static logic.constant.Consts.THIRD_RANGING_ROOK_CONFIG;
import static logic.constant.Consts.TRUE_STATIC_ROOK_CONFIG;
import static logic.strategy.StrategyType.LEFT_BEAR_HOLE;
import static logic.strategy.StrategyType.LEFT_GOLD_FORTRESS;
import static logic.strategy.StrategyType.LEFT_MINO;
import static logic.strategy.StrategyType.LEFT_SILVER_CROWN;
import static logic.strategy.StrategyType.RIGHT_BEAR_HOLE;
import static logic.strategy.StrategyType.RIGHT_GOLD_FORTRESS;
import static logic.strategy.StrategyType.RIGHT_MINO;
import static logic.strategy.StrategyType.RIGHT_SILVER_CROWN;
import static logic.strategy.StrategyType.THIRD_RANGING_ROOK;
import static logic.strategy.StrategyType.TRUE_STATIC_ROOK;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import javafx.geometry.Point2D;
import logic.piece.King;
import logic.strategy.StrategyType;

public class RookPosition extends Passive {

    private List<StrategyType> matchingCastles;

    private RookPosition(StrategyType type, King owner, Map<Point2D, Point2D> positions, List<StrategyType> matchingCastles) {
        super(type, owner, positions);
        this.matchingCastles = matchingCastles;
    }

    @Override
    public double getScore() {
        return 100;
    }

    public static class TrueStaticRook extends RookPosition {

        public TrueStaticRook(King owner) {
            super(TRUE_STATIC_ROOK, owner, TRUE_STATIC_ROOK_CONFIG,
                    Arrays.asList(
                            LEFT_GOLD_FORTRESS,
                            LEFT_MINO,
                            LEFT_SILVER_CROWN,
                            LEFT_BEAR_HOLE));
        }
    }

    public static class ThirdRangingRook extends RookPosition {

        public ThirdRangingRook(King owner) {
            super(THIRD_RANGING_ROOK, owner, THIRD_RANGING_ROOK_CONFIG,
                    Arrays.asList(
                            RIGHT_GOLD_FORTRESS,
                            RIGHT_MINO,
                            RIGHT_SILVER_CROWN,
                            RIGHT_BEAR_HOLE));
        }
    }

    public List<StrategyType> getMatchingCastles() {
        return matchingCastles;
    }
}
