package logic.strategy.passive;

import static logic.constant.Consts.LEFT_BEAR_HOLE_CONFIG;
import static logic.constant.Consts.LEFT_GOLD_FORTRESS_CONFIG;
import static logic.constant.Consts.LEFT_MINO_CONFIG;
import static logic.constant.Consts.LEFT_SILVER_CROWN_CONFIG;
import static logic.constant.Consts.RIGHT_BEAR_HOLE_CONFIG;
import static logic.constant.Consts.RIGHT_GOLD_FORTRESS_CONFIG;
import static logic.constant.Consts.RIGHT_MINO_CONFIG;
import static logic.constant.Consts.RIGHT_SILVER_CROWN_CONFIG;
import static logic.strategy.StrategyType.LEFT_BEAR_HOLE;
import static logic.strategy.StrategyType.LEFT_GOLD_FORTRESS;
import static logic.strategy.StrategyType.LEFT_MINO;
import static logic.strategy.StrategyType.LEFT_SILVER_CROWN;
import static logic.strategy.StrategyType.RIGHT_BEAR_HOLE;
import static logic.strategy.StrategyType.RIGHT_GOLD_FORTRESS;
import static logic.strategy.StrategyType.RIGHT_MINO;
import static logic.strategy.StrategyType.RIGHT_SILVER_CROWN;
import java.util.Map;
import javafx.geometry.Point2D;
import logic.piece.King;
import logic.strategy.StrategyType;

public class Castle extends Passive {

    private Castle(StrategyType type, King owner, Map<Point2D, Point2D> positions) {
        super(type, owner, positions);
    }

    @Override
    public double getScore() {
        return 75;
    }

    public static class LeftGoldFortress extends Castle {

        public LeftGoldFortress(King owner) {
            super(LEFT_GOLD_FORTRESS, owner, LEFT_GOLD_FORTRESS_CONFIG);
        }
    }

    public static class RightGoldFortress extends Castle {

        public RightGoldFortress(King owner) {
            super(RIGHT_GOLD_FORTRESS, owner, RIGHT_GOLD_FORTRESS_CONFIG);
        }
    }

    public static class LeftMino extends Castle {

        public LeftMino(King owner) {
            super(LEFT_MINO, owner, LEFT_MINO_CONFIG);
        }
    }

    public static class RightMino extends Castle {

        public RightMino(King owner) {
            super(RIGHT_MINO, owner, RIGHT_MINO_CONFIG);
        }
    }

    public static class LeftSilverCrown extends Castle {

        public LeftSilverCrown(King owner) {
            super(LEFT_SILVER_CROWN, owner, LEFT_SILVER_CROWN_CONFIG);
        }
    }

    public static class RightSilverCrown extends Castle {

        public RightSilverCrown(King owner) {
            super(RIGHT_SILVER_CROWN, owner, RIGHT_SILVER_CROWN_CONFIG);
        }
    }

    public static class LeftBearHole extends Castle {

        public LeftBearHole(King owner) {
            super(LEFT_BEAR_HOLE, owner, LEFT_BEAR_HOLE_CONFIG);
        }
    }

    public static class RightBearHole extends Castle {

        public RightBearHole(King owner) {
            super(RIGHT_BEAR_HOLE, owner, RIGHT_BEAR_HOLE_CONFIG);
        }
    }
}
