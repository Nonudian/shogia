package logic.strategy;

import logic.move.Move;
import logic.strategy.passive.Passive;
import logic.util.Scorable;

import java.util.Random;

public class StrategyPair implements Comparable<StrategyPair>, Scorable {

    private Strategy strategy;
    private Move move;

    public StrategyPair(Strategy strategy, Move move) {
        this.strategy = strategy;
        this.move = move;
    }

    public Strategy first() {
        return strategy;
    }
    public Move second() {
        return move;
    }

    private int compareSameStrategy(StrategyPair o) {
        if (strategy instanceof Passive) {
            int oLeft = ((Passive) o.strategy).leftoverMoveCounter(o.move);
            int thisLeft = ((Passive) strategy).leftoverMoveCounter(move);
            if(oLeft > thisLeft) return 1;
            else if(oLeft < thisLeft) return -1;
            else return compareDefaultStrategy(o);
        }
        return compareDefaultStrategy(o);
    }

    private int compareDefaultStrategy(StrategyPair o) {
        double comp = o.getScore() - getScore();
        if(comp > 0) return 1;
        else if(comp < 0) return -1;
        else return new Random().nextBoolean() ? -1 : 1;
    }

    @Override
    public int compareTo(StrategyPair o) {
        if(o.strategy == null && strategy == null) return compareDefaultStrategy(o);
        if(o.strategy == null) return -1;
        if(strategy == null) return 1;
        if(o.strategy.type == strategy.type) return compareSameStrategy(o);
        return compareDefaultStrategy(o);
    }

    @Override
    public double getScore() {
        if(strategy != null) return strategy.getScore() + move.getScore();
        return move.getScore();
    }
}
