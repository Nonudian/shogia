package logic.strategy;

import java.util.List;

import logic.piece.Piece;

public interface Collective {

    void setAllies();

    List<Piece> getAllies();

    List<Piece> getOldAllies();

    void removeFallen(Piece fallen);
}
