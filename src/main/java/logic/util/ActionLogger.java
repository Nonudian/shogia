package logic.util;

import javafx.geometry.Point2D;
import logic.piece.Piece;

import java.util.logging.Formatter;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

import static control.ShogIAApp.currentTeam;
import static control.ShogIAApp.turn;

public class ActionLogger {

    private static final Logger logger = Logger.getLogger(ActionLogger.class.getName());

    public static void setupLogger() {
        Formatter formatter = new Formatter() {

            @Override
            public String format(LogRecord record) {
                return "\n[TURN "
                        + turn
                        + " - "
                        + currentTeam.name()
                        + "] | "
                        + currentTeam.actives.size()
                        + " piece(s) on board | "
                        + currentTeam.inactives.size()
                        + " piece(s) on hand\n"
                        + record.getMessage()
                        + ".\n";
            }
        };

        ConsoleHandler handler = new ConsoleHandler();
        handler.setFormatter(formatter);

        logger.setLevel(Level.INFO);
        logger.addHandler(handler);
        logger.setUseParentHandlers(false);
    }

    private static String getEnd() {
        return currentTeam.name() + " team won ! The opposite ";
    }

    private static String getMove(Piece movingPiece, Point2D startPosition, Point2D endPosition) {
        return movingPiece.getClass().getSimpleName()
                + " piece moved from "
                + startPosition
                + " to "
                + endPosition;
    }

    private static String getCapture(Piece targetPiece, Piece movingPiece, Point2D startPosition) {
        return targetPiece.getClass().getSimpleName()
            + " piece at "
            + targetPiece.getPosition()
            + " is caught by a "
            + movingPiece.getClass().getSimpleName()
            + " piece from "
            + startPosition;
    }

    private static String getDrop(Piece droppingPiece, Point2D endPosition) {
        return droppingPiece.getClass().getSimpleName()
            + " piece dropped from hand to "
            + endPosition;
    }

    public static void end(Piece targetPiece, Piece movingPiece, Point2D startPosition) {
        logger.info(getEnd() + getCapture(targetPiece, movingPiece, startPosition));
    }

    public static void capture(Piece targetPiece, Piece movingPiece, Point2D startPosition) {
        logger.info(
            getMove(movingPiece, startPosition, targetPiece.getPosition())
                + "\n"
                + getCapture(targetPiece, movingPiece, startPosition));
    }

    public static void move(Piece movingPiece, Point2D startPosition, Point2D endPosition) {
        logger.info(getMove(movingPiece, startPosition, endPosition));
    }

    public static void drop(Piece droppingPiece, Point2D endPosition) {
        logger.info(getDrop(droppingPiece, endPosition));
    }
}
