package logic.util;

import static java.util.function.Predicate.isEqual;
import static java.util.function.Predicate.not;
import static logic.constant.Consts.HEIGHT;
import static logic.constant.Consts.WIDTH;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javafx.geometry.Point2D;
import logic.move.Motion;
import logic.piece.King;
import logic.piece.Pawn;
import logic.piece.Piece;
import logic.piece.PieceFactory;

public enum Team {

    TOP(1, 0),
    BOTTOM(-1, 8);

    public final int orientationFactor;
    public final int origin;
    public List<Piece> actives;
    public List<Piece> inactives = new ArrayList<>();
    public Piece lastFallen;

    public Map<String, Integer> statistics = new HashMap<>();

    public List<Point2D> unoccupied = new ArrayList<>();
    public List<Double> nifu = new ArrayList<>();
    public Map<Point2D, Integer> checkmated = new HashMap<>();

    Team(int orientationFactor, int origin) {
        this.orientationFactor = orientationFactor;
        this.origin = origin;
        this.actives = PieceFactory.buildPieces(this);
        this.getKing().setExecutor();
    }

    public Team getOpposite() {
        return Team.values()[1 - ordinal()];
    }
    public int size() {
        return getPieces().size();
    }

    public void capture(Piece piece) {
        getOpposite().actives.remove(piece);
        inactives.add(piece);
    }
    public void drop(Piece piece) {
        inactives.remove(piece);
        actives.add(piece);
    }

    public Optional<Piece> getPieceByPosition(Point2D position) {
        return actives.stream().filter(piece -> piece.getPosition().equals(position)).findFirst();
    }

    //WARNING: begin at BOTTOM (not at TOP, as used in PieceFactory)
    public Point2D getTruePosition(Point2D relativePosition) {
        return new Point2D(
                getOpposite().origin + getOpposite().orientationFactor * relativePosition.getX(),
                getOpposite().origin + getOpposite().orientationFactor * relativePosition.getY());
    }

    public List<Piece> getActiveEnemies() {
        return Stream.of(getOpposite().actives)
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }
    public List<Piece> getPieces() {
        return Stream.of(actives, inactives)
            .flatMap(Collection::stream)
            .collect(Collectors.toList());
    }
    public King getKing() {
        return actives.stream()
            .filter(King.class::isInstance)
            .map(King.class::cast)
            .findFirst()
            .get();
    }
    public List<Piece> getAllActiveExceptedKing() {
        return actives.stream()
                .filter(not(King.class::isInstance))
                .collect(Collectors.toList());
    }
    public List<Piece> getAllExcepted(Piece piece) {
        return Stream.of(actives, inactives)
                .flatMap(Collection::stream)
                .filter(not(isEqual(piece)))
                .collect(Collectors.toList());
    }
    public List<Integer> getPiecesIds(List<Piece> pieces) {
        return pieces.stream().map(Piece::getId).collect(Collectors.toList());
    }

    public Optional<Piece> getEnemyFrom(Point2D position) {
        return getOpposite().actives.stream().filter(enemy -> enemy.getPosition().equals(position)).findFirst();
    }
    public List<Point2D> getUnoccupied() {
        return unoccupied;
    }

    public void update() {
        updateUnoccupied();
        updateNifu();
        updateCheckmated();
    }
    private void updateUnoccupied() {
        unoccupied.clear();
        for (int y = 0; y < HEIGHT; y++) {
            for (int x = 0; x < WIDTH; x++) {
                Point2D position = new Point2D(x, y);
                if (!isPresentEnemy(position) && !isPresentAlly(position)) unoccupied.add(position);
            }
        }
    }
    private void updateNifu() {
        nifu.clear();
        actives.stream()
                .filter(Pawn.class::isInstance)
                .map(Pawn.class::cast)
                .filter(not(Pawn::isPromoted))
                .map(Piece::getPosition)
                .map(Point2D::getX)
                .distinct()
                .forEach(nifu::add);
    }
    private void updateCheckmated() {
        checkmated.clear();

        getOpposite().getKing().getCurrentMotions().stream()
                .map(Motion::getFinalPosition)
                .forEach(position -> checkmated.put(position, 0));
        actives.stream()
                .map(Piece::getCurrentMotions)
                .flatMap(Collection::stream)
                .map(Motion::getFinalPosition)
                .filter(checkmated::containsKey)
                .forEach(position -> checkmated.merge(position, -1, Integer::sum));
        getOpposite().getAllActiveExceptedKing().stream()
                .map(Piece::getCurrentMotions)
                .flatMap(Collection::stream)
                .map(Motion::getFinalPosition)
                .filter(checkmated::containsKey)
                .forEach(position -> checkmated.merge(position, 1, Integer::sum));
    }

    public boolean isPresentEnemy(Point2D position) {
        return getOpposite().isPresentAlly(position);
    }
    public boolean isPresentAlly(Point2D position) {
        return actives.stream().map(Piece::getPosition).anyMatch(isEqual(position));
    }
    public boolean isPresentNifu(Point2D position) {
        return nifu.contains(position.getX());
    }
    public boolean isPresentCheckmate(Point2D position) {
        return checkmated.containsKey(position) && checkmated.get(position) < 0;
    }
}
