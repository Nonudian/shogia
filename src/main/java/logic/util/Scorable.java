package logic.util;

public interface Scorable {
    double getScore();
}
