package logic.message;

import logic.piece.Piece;

public class Message {
    protected Piece sender;
    protected MessageType type;

    public Message(Piece sender, MessageType type) {
        this.sender = sender;
        this.type = type;
    }

    public Piece getSender() {
        return sender;
    }

    public MessageType getType() {
        return type;
    }
}
