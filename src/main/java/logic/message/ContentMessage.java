package logic.message;

import logic.piece.Piece;

public class ContentMessage<T> extends Message {
    protected T content;

    public ContentMessage(Piece sender, MessageType type, T content) {
        super(sender, type);
        this.content = content;
    }

    public T getContent(){
        return this.content;
    }
}
