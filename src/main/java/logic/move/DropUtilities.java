package logic.move;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import javafx.geometry.Point2D;
import logic.util.Team;
import static java.util.function.Predicate.not;

public final class DropUtilities {

    private DropUtilities() {}

    public static List<Motion> defaultPositions(Team team) {
        return team.getUnoccupied().stream().map(Motion::new).collect(Collectors.toList());
    }

    public static List<Motion> knightPositions(Team team) {
        return defaultPositions(team)
                .stream()
                .filter(motion -> !ForwardUtilities.knightPositions(motion.getFinalPosition(), team).isEmpty())
                .collect(Collectors.toList());
    }

    public static List<Motion> pawnPositions(Team team) {
        return defaultPositions(team)
                .stream()
                .map(motion -> {
                    Point2D position = motion.getFinalPosition();
                    if (!ForwardUtilities.pawnPositions(position, team).isEmpty()
                            && !team.isPresentNifu(position)
                            && !team.isPresentCheckmate(position))
                        return motion;
                    return null;
                })
                .filter(not(Objects::isNull))
                .collect(Collectors.toList());
    }

    public static List<Motion> lancePositions(Team team) {
        return defaultPositions(team)
                .stream()
                .filter(motion -> !ForwardUtilities.lancePositions(motion.getFinalPosition(), team).isEmpty())
                .collect(Collectors.toList());
    }
}
