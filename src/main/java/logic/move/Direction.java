package logic.move;

import java.util.Arrays;
import java.util.List;

import java.util.stream.Collectors;
import javafx.geometry.Point2D;
import static java.util.function.Predicate.not;

public enum Direction {

    NORTH(new Point2D(0, 1)),
    SOUTH(new Point2D(0, -1)),
    EAST(new Point2D(1, 0)),
    WEST(new Point2D(-1, 0)),
    NORTH_EAST(new Point2D(1, 1)),
    NORTH_WEST(new Point2D(-1, 1)),
    SOUTH_EAST(new Point2D(1, -1)),
    SOUTH_WEST(new Point2D(-1, -1));

    private Point2D coords;

    Direction(Point2D coords) {
        this.coords = coords;
    }

    public static List<Direction> allDirections() {
        return Arrays.asList(Direction.values());
    }

    public static List<Direction> straightDirections() {
        return Arrays.asList(NORTH, SOUTH, EAST, WEST);
    }

    public static List<Direction> diagonalDirections() {
        return Arrays.asList(NORTH_EAST, NORTH_WEST, SOUTH_EAST, SOUTH_WEST);
    }

    public static List<Direction> directionsWithout(Direction... toSubtract) {
        List<Direction> dirToSubtract = Arrays.asList(toSubtract);
        return Arrays.stream(Direction.values())
                .filter(not(dirToSubtract::contains))
                .collect(Collectors.toList());
    }

    public Point2D getCoords(int orientationFactor) {
        return coords.multiply(orientationFactor);
    }
}
