package logic.move;

import javafx.geometry.Point2D;

public class Motion {

    private Point2D finalPosition;
    private Direction direction;

    public Motion(Point2D finalPosition, Direction direction) {
        this.finalPosition = finalPosition;
        this.direction = direction;
    }

    public Motion(Point2D finalPosition) {
        this.finalPosition = finalPosition;
    }

    public Point2D getFinalPosition() {
        return finalPosition;
    }

    public Direction getDirection() {
        return direction;
    }
}
