package logic.move;

import static logic.move.Move.MoveState.CAPTURING_AND_PROMOTING_MOVE;
import static logic.move.Move.MoveState.CAPTURING_MOVE;
import static logic.move.Move.MoveState.DROP;
import static logic.move.Move.MoveState.PROMOTING_MOVE;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import javafx.geometry.Point2D;
import logic.piece.Piece;
import logic.piece.Promotable;
import logic.util.Scorable;

public class Move implements Comparable<Move>, Scorable {

    private final Piece actor;
    private final Motion motion;
    private MoveState state;
    private List<Piece> threats = new ArrayList<>();
    private Piece target;
    private Map<Direction, Piece> potentialTargets = new HashMap<>();

    public Move(Piece actor, Motion motion, boolean drop) {
        this.actor = actor;
        this.motion = motion;
        this.state = drop ? MoveState.DROP : MoveState.FORWARD;
        initPotentialTargets();
    }

    public Move(Move move) {
        this.actor = move.actor;
        this.motion = move.motion;
        this.state = move.state;
        this.threats = move.threats;
        this.target = move.target;
        this.potentialTargets = move.potentialTargets;
    }

    public Piece getActor() {
        return actor;
    }
    public Point2D getFinalPosition() {
        return motion.getFinalPosition();
    }
    public Direction getDirection() {
        return motion.getDirection();
    }
    public MoveState getState() {
        return state;
    }
    public List<Piece> getThreats() {
        return threats;
    }
    public Piece getTarget() {
        return target;
    }
    public Map<Direction, Piece> getPotentialTargets() {
        return potentialTargets;
    }
    public boolean hasPotentialTargets() {
        return potentialTargets.size() > 0;
    }

    private void initPotentialTargets() {
        actor.getForwardPositions(motion.getFinalPosition(), actor.getPosition()).forEach(motion ->
                actor.getTeam().getOpposite().getPieceByPosition(motion.getFinalPosition())
                        .ifPresent(piece -> potentialTargets.put(motion.getDirection(), piece)));
    }

    public boolean hasTarget() {
        return Optional.ofNullable(target).isPresent();
    }
    public boolean isRisky() {
        return !threats.isEmpty();
    }

    public void withRisk(List<Piece> threats) {
        this.threats = threats;
    }
    public void withCapture(Piece target) {
        this.target = target;
        state = (state == PROMOTING_MOVE) ? CAPTURING_AND_PROMOTING_MOVE : CAPTURING_MOVE;
    }
    public void withPromotion() {
        state = (state == CAPTURING_MOVE) ? CAPTURING_AND_PROMOTING_MOVE : PROMOTING_MOVE;
    }

    @Override
    public int compareTo(Move o) {
        double comp = o.getScore() - getScore();
        if(comp > 0) return 1;
        else if(comp < 0) return -1;
        else return 0;
    }

    @Override
    public double getScore() {
        return getCaptureScore() - getThreatScore() + getPotentialTargetScore() + getValuationUpdateScore();
    }

    private double getThreatScore() {
        if(actor.isThreatened() && !isRisky()) return -actor.getValuation()*actor.getThreats().get().size();
        if(isRisky()) return actor.getValuation()*threats.size();
        return 0;
    }

    private double getCaptureScore() {
        if(hasTarget()) return target.getValuation();
        return 0;
    }

    private double getPotentialTargetScore() {
        if(hasPotentialTargets()) return potentialTargets.values().stream().map(Piece::getValuation).min(Double::compare).orElse(0.);
        return 0;
    }

    private double getValuationUpdateScore() {
        double valuation = actor.getValuation();
        double updated = valuation;
        if(state == DROP) updated = actor.getInHandValuation();
        if(state == PROMOTING_MOVE || state == CAPTURING_AND_PROMOTING_MOVE) updated = ((Promotable)actor).getPromotionValuation();
        return updated - valuation;
    }

    public enum MoveState {
        FORWARD,
        PROMOTING_MOVE,
        CAPTURING_MOVE,
        CAPTURING_AND_PROMOTING_MOVE,
        DROP
    }
}
