package logic.move;

import static java.util.function.Predicate.not;
import static logic.constant.Consts.HEIGHT;
import static logic.constant.Consts.WIDTH;
import static logic.move.Direction.EAST;
import static logic.move.Direction.NORTH;
import static logic.move.Direction.NORTH_EAST;
import static logic.move.Direction.NORTH_WEST;
import static logic.move.Direction.SOUTH;
import static logic.move.Direction.SOUTH_EAST;
import static logic.move.Direction.SOUTH_WEST;
import static logic.move.Direction.WEST;
import static logic.move.Direction.allDirections;
import static logic.move.Direction.diagonalDirections;
import static logic.move.Direction.directionsWithout;
import static logic.move.Direction.straightDirections;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javafx.geometry.Point2D;
import logic.util.Team;

public final class ForwardUtilities {

    private ForwardUtilities() {}

    private static boolean isExistingCoords(Point2D position) {
        return position.getX() > -1
                && position.getY() > -1
                && position.getX() < WIDTH
                && position.getY() < HEIGHT;
    }

    private static List<Motion> staticSearch(Point2D startPosition, List<Direction> directions, Team team, Point2D... toExclude) {
        return directions.stream()
                .map(direction -> {
                    Point2D position = startPosition.add(direction.getCoords(team.orientationFactor));

                    if (isExcludedPosition(position, toExclude) || (isExistingCoords(position) && !team.isPresentAlly(position)))
                        return new Motion(position, direction);
                    return null;
                })
                .filter(not(Objects::isNull))
                .collect(Collectors.toList());
    }

    private static List<Motion> dynamicSearch(Point2D startPosition, List<Direction> directions, Team team, Point2D... toExclude) {
        return directions.stream()
                .map(direction -> searchPositions(startPosition, direction, new ArrayList<>(), team, toExclude))
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }

    private static List<Motion> searchPositions(Point2D startPosition, Direction direction, List<Motion> motions, Team team, Point2D... toExclude) {
        startPosition = startPosition.add(direction.getCoords(team.orientationFactor));

        if (isExcludedPosition(startPosition, toExclude) || (isExistingCoords(startPosition) && !team.isPresentAlly(startPosition))) {
            motions.add(new Motion(startPosition, direction));

            if (team.isPresentEnemy(startPosition)) return motions;
            return searchPositions(startPosition, direction, motions, team, toExclude);
        }
        return motions;
    }

    private static boolean isExcludedPosition(Point2D startPosition, Point2D... toExclude) {
        for (Point2D point2D : toExclude) {
            if (startPosition.equals(point2D)) return true;
        }
        return false;
    }

    public static List<Motion> kingPositions(Point2D startPosition, Team team, Point2D... toExclude) {
        return staticSearch(startPosition, allDirections(), team, toExclude);
    }

    public static List<Motion> goldPositions(Point2D startPosition, Team team, Point2D... toExclude) {
        return staticSearch(startPosition, directionsWithout(SOUTH_EAST, SOUTH_WEST), team, toExclude);
    }

    public static List<Motion> silverPositions(Point2D startPosition, Team team, Point2D... toExclude) {
        return staticSearch(startPosition, directionsWithout(EAST, WEST, SOUTH), team, toExclude);
    }

    public static List<Motion> knightPositions(Point2D startPosition, Team team, Point2D... toExclude) {
        return staticSearch(startPosition.add(NORTH.getCoords(team.orientationFactor)), Arrays.asList(NORTH_EAST, NORTH_WEST), team, toExclude);
    }

    public static List<Motion> pawnPositions(Point2D startPosition, Team team, Point2D... toExclude) {
        return staticSearch(startPosition, Collections.singletonList(NORTH), team, toExclude);
    }

    public static List<Motion> lancePositions(Point2D startPosition, Team team, Point2D... toExclude) {
        return dynamicSearch(startPosition, Collections.singletonList(NORTH), team, toExclude);
    }

    public static List<Motion> rookPositions(Point2D startPosition, Team team, Point2D... toExclude) {
        return dynamicSearch(startPosition, straightDirections(), team, toExclude);
    }

    public static List<Motion> bishopPositions(Point2D startPosition, Team team, Point2D... toExclude) {
        return dynamicSearch(startPosition, diagonalDirections(), team, toExclude);
    }

    public static List<Motion> promotedRookPositions(Point2D startPosition, Team team, Point2D... toExclude) {
        return Stream.of(rookPositions(startPosition, team, toExclude), goldPositions(startPosition, team, toExclude))
                .flatMap(Collection::stream)
                .distinct()
                .collect(Collectors.toList());
    }

    public static List<Motion> promotedBishopPositions(Point2D startPosition, Team team, Point2D... toExclude) {
        return Stream.of(bishopPositions(startPosition, team, toExclude), goldPositions(startPosition, team, toExclude))
                .flatMap(Collection::stream)
                .distinct()
                .collect(Collectors.toList());
    }
}
