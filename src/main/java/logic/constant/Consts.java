package logic.constant;

import java.util.HashMap;
import java.util.Map;

import javafx.geometry.Point2D;

public final class Consts {

    public static final int HEIGHT = 9;
    public static final int WIDTH = 9;

    public static boolean ENGLISH_NOTATION = true;
    public static final String LANCE = ENGLISH_NOTATION ? "L" : "香";
    public static final String KNIGHT = ENGLISH_NOTATION ? "N" : "桂";
    public static final String SILVER = ENGLISH_NOTATION ? "S" : "銀";
    public static final String GOLD = ENGLISH_NOTATION ? "G" : "金";
    public static final String KING = ENGLISH_NOTATION ? "K" : "王";
    public static final String ROOK = ENGLISH_NOTATION ? "R" : "飛";
    public static final String BISHOP = ENGLISH_NOTATION ? "B" : "角";
    public static final String PAWN = ENGLISH_NOTATION ? "P" : "歩";

    public static final String[][] INITIAL_LAYOUT
            = { { LANCE, KNIGHT, SILVER, GOLD, KING, GOLD, SILVER, KNIGHT, LANCE },
                { " ", ROOK, " ", " ", " ", " ", " ", BISHOP, " " },
                { PAWN, PAWN, PAWN, PAWN, PAWN, PAWN, PAWN, PAWN, PAWN } };


    /***********************************
     *          CASTLE CONFIGS         *
     ***********************************/

    //GOLD FORTRESS (FORTRESS VARIANT)
    public static final Map<Point2D, Point2D> LEFT_GOLD_FORTRESS_CONFIG = new HashMap<>() {{
        //put(new Point2D(0, 8), new Point2D(0, 8)); //LANCE
        //put(new Point2D(0, 6), new Point2D(0, 6)); //PAWN
        //put(new Point2D(1, 8), new Point2D(1, 8)); //KNIGHT
        put(new Point2D(1, 7), new Point2D(4, 8)); //KING
        //put(new Point2D(1, 6), new Point2D(1, 6)); //PAWN
        put(new Point2D(2, 7), new Point2D(3, 8)); //GOLD
        put(new Point2D(2, 6), new Point2D(2, 8)); //SILVER
        put(new Point2D(2, 5), new Point2D(2, 6)); //PAWN
        put(new Point2D(3, 7), new Point2D(1, 7)); //BISHOP
        put(new Point2D(3, 6), new Point2D(5, 8)); //GOLD
        put(new Point2D(3, 5), new Point2D(3, 6)); //PAWN
        put(new Point2D(4, 5), new Point2D(4, 6)); //PAWN
    }};
    public static final Map<Point2D, Point2D> RIGHT_GOLD_FORTRESS_CONFIG = new HashMap<>() {{
        put(new Point2D(4, 5), new Point2D(4, 6)); //PAWN
        put(new Point2D(5, 6), new Point2D(3, 8)); //GOLD
        put(new Point2D(5, 5), new Point2D(5, 6)); //PAWN
        put(new Point2D(6, 7), new Point2D(5, 8)); //GOLD
        put(new Point2D(6, 6), new Point2D(6, 8)); //SILVER
        put(new Point2D(6, 5), new Point2D(6, 6)); //PAWN
        //put(new Point2D(7, 8), new Point2D(7, 8)); //KNIGHT
        put(new Point2D(7, 7), new Point2D(4, 8)); //KING
        //put(new Point2D(7, 6), new Point2D(7, 6)); //PAWN
        //put(new Point2D(8, 8), new Point2D(8, 8)); //LANCE
        //put(new Point2D(8, 6), new Point2D(8, 6)); //PAWN
    }};

    //MINO
    public static final Map<Point2D, Point2D> LEFT_MINO_CONFIG = new HashMap<>() {{
        //put(new Point2D(0, 8), new Point2D(0, 8)); //LANCE
        put(new Point2D(0, 5), new Point2D(0, 6)); //PAWN
        //put(new Point2D(1, 8), new Point2D(1, 8)); //KNIGHT
        put(new Point2D(1, 7), new Point2D(4, 8)); //KING
        //put(new Point2D(1, 6), new Point2D(1, 6)); //PAWN
        put(new Point2D(2, 7), new Point2D(2, 8)); //SILVER
        put(new Point2D(2, 6), new Point2D(1, 7)); //BISHOP
        put(new Point2D(2, 5), new Point2D(2, 6)); //PAWN
        //put(new Point2D(3, 8), new Point2D(3, 8)); //GOLD
        //put(new Point2D(3, 6), new Point2D(3, 6)); //PAWN
        put(new Point2D(4, 7), new Point2D(5, 8)); //GOLD
        //put(new Point2D(4, 6), new Point2D(4, 6)); //PAWN
    }};
    public static final Map<Point2D, Point2D> RIGHT_MINO_CONFIG = new HashMap<>() {{
        put(new Point2D(4, 7), new Point2D(3, 8)); //GOLD
        //put(new Point2D(4, 6), new Point2D(4, 6)); //PAWN
        //put(new Point2D(5, 8), new Point2D(5, 8)); //GOLD
        //put(new Point2D(5, 6), new Point2D(5, 6)); //PAWN
        put(new Point2D(6, 7), new Point2D(6, 8)); //SILVER
        //put(new Point2D(6, 6), new Point2D(6, 6)); //PAWN
        //put(new Point2D(7, 8), new Point2D(7, 8)); //KNIGHT
        put(new Point2D(7, 7), new Point2D(4, 8)); //KING
        //put(new Point2D(7, 6), new Point2D(7, 6)); //PAWN
        //put(new Point2D(8, 8), new Point2D(8, 8)); //LANCE
        put(new Point2D(8, 5), new Point2D(8, 6)); //PAWN
    }};

    //SILVER CROWN (MINO VARIANT)
    public static final Map<Point2D, Point2D> LEFT_SILVER_CROWN_CONFIG = new HashMap<>() {{
        //put(new Point2D(0, 8), new Point2D(0, 8)); //LANCE
        put(new Point2D(0, 5), new Point2D(0, 6)); //PAWN
        //put(new Point2D(1, 8), new Point2D(1, 8)); //KNIGHT
        put(new Point2D(1, 7), new Point2D(4, 8)); //KING
        put(new Point2D(1, 6), new Point2D(2, 8)); //SILVER
        put(new Point2D(1, 5), new Point2D(1, 6)); //PAWN
        put(new Point2D(2, 7), new Point2D(3, 8)); //GOLD
        put(new Point2D(2, 6), new Point2D(6, 8)); //SILVER
        put(new Point2D(2, 5), new Point2D(2, 6)); //PAWN
        put(new Point2D(3, 7), new Point2D(1, 7)); //BISHOP
        put(new Point2D(3, 6), new Point2D(5, 8)); //GOLD
        put(new Point2D(3, 5), new Point2D(3, 6)); //PAWN
    }};
    public static final Map<Point2D, Point2D> RIGHT_SILVER_CROWN_CONFIG = new HashMap<>() {{
        put(new Point2D(5, 6), new Point2D(3, 8)); //GOLD
        put(new Point2D(5, 5), new Point2D(5, 6)); //PAWN
        put(new Point2D(6, 7), new Point2D(5, 8)); //GOLD
        put(new Point2D(6, 6), new Point2D(7, 8)); //KNIGHT
        put(new Point2D(6, 5), new Point2D(6, 6)); //PAWN
        put(new Point2D(7, 7), new Point2D(4, 8)); //KING
        put(new Point2D(7, 6), new Point2D(6, 8)); //SILVER
        put(new Point2D(7, 5), new Point2D(7, 6)); //PAWN
        //put(new Point2D(8, 8), new Point2D(8, 8)); //LANCE
        put(new Point2D(8, 5), new Point2D(8, 6)); //PAWN
    }};

    //BEAR IN THE HOLE
    public static final Map<Point2D, Point2D> LEFT_BEAR_HOLE_CONFIG = new HashMap<>() {{
        put(new Point2D(0, 8), new Point2D(4, 8)); //KING
        put(new Point2D(0, 7), new Point2D(0, 8)); //LANCE
        //put(new Point2D(0, 6), new Point2D(0, 6)); //PAWN
        //put(new Point2D(1, 8), new Point2D(1, 8)); //KNIGHT
        put(new Point2D(1, 7), new Point2D(2, 8)); //SILVER
        //put(new Point2D(1, 6), new Point2D(1, 6)); //PAWN
        put(new Point2D(2, 8), new Point2D(3, 8)); //GOLD
        put(new Point2D(2, 7), new Point2D(5, 8)); //GOLD
        put(new Point2D(2, 6), new Point2D(1, 7)); //BISHOP
        put(new Point2D(2, 5), new Point2D(2, 6)); //PAWN
        //put(new Point2D(3, 6), new Point2D(3, 6)); //PAWN
    }};
    public static final Map<Point2D, Point2D> RIGHT_BEAR_HOLE_CONFIG = new HashMap<>() {{
        //put(new Point2D(5, 6), new Point2D(5, 6)); //PAWN
        put(new Point2D(6, 8), new Point2D(5, 8)); //GOLD
        put(new Point2D(6, 7), new Point2D(3, 8)); //GOLD
        //put(new Point2D(6, 6), new Point2D(6, 6)); //PAWN
        //put(new Point2D(7, 8), new Point2D(7, 8)); //KNIGHT
        put(new Point2D(7, 7), new Point2D(6, 8)); //SILVER
        //put(new Point2D(7, 6), new Point2D(7, 6)); //PAWN
        put(new Point2D(8, 8), new Point2D(4, 8)); //KING
        put(new Point2D(8, 7), new Point2D(8, 8)); //LANCE
        //put(new Point2D(8, 6), new Point2D(8, 6)); //PAWN
    }};


    /******************************************
     *          ROOK POSITION CONFIGS         *
     ******************************************/

    //STATIC ROOK
    public static final Map<Point2D, Point2D> TRUE_STATIC_ROOK_CONFIG = new HashMap<>() {{
        //put(new Point2D(7, 7), new Point2D(7, 7)); //ROOK
        put(new Point2D(7, 5), new Point2D(7, 6)); //PAWN
    }};

    //RANGING ROOK
    public static final Map<Point2D, Point2D> THIRD_RANGING_ROOK_CONFIG = new HashMap<>() {{
        put(new Point2D(2, 7), new Point2D(7, 7)); //ROOK
        put(new Point2D(2, 5), new Point2D(2, 6)); //PAWN
        put(new Point2D(3, 5), new Point2D(3, 6)); //PAWN
    }};

    private Consts() {}
}
