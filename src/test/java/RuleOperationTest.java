import static control.ShogIAApp.currentTeam;
import static org.junit.Assert.assertEquals;
import logic.util.Team;
import org.junit.Test;

public class RuleOperationTest {

    private Team[] teams = Team.values();

    /******************************************************
     *                     RULE TESTS                     *
     ******************************************************/

    @Test
    public void testAllFormulas() {
        currentTeam = teams[0];
        currentTeam.update();

        currentTeam.getPieces().forEach(piece -> {
            int computedCount = piece.getCurrentMotions().size();
            int expectedCount = 0;
            switch (piece.getLetter()) {
                case "L":
                case "香":
                    expectedCount = 1;
                    break;
                case "N":
                case "桂":
                    expectedCount = 0;
                    break;
                case "S":
                case "銀":
                    expectedCount = 2;
                    break;
                case "G":
                case "金":
                    expectedCount = 3;
                    break;
                case "K":
                case "王":
                    expectedCount = 3;
                    break;
                case "R":
                case "飛":
                    expectedCount = 6;
                    break;
                case "B":
                case "角":
                    expectedCount = 0;
                    break;
                case "P":
                case "歩":
                    expectedCount = 1;
                    break;
            }
            assertEquals(computedCount, expectedCount);
        });
    }
}
